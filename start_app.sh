#!/bin/sh

APP_NAME="cinema.agr.j"
APP_CODE=cinema.agr
APP_HOME=C:/Work/cinema.agr.j
APP_PID=$APP_HOME/$APP_CODE.pid
APP_JAR=$APP_HOME/cinema.agr.j.jar
APP_CONF=$APP_HOME/config.xml
APP_LOG=$APP_HOME/log4j2.xml

case $1 in
    start)
        echo "Starting $APP_NAME server ..."
        if [ ! -f $APP_PID ]; then
            java.exe -XX:OnError="start_app.sh stop" -server -Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager -Dlog4j.configurationFile=file:$APP_LOG -jar $APP_JAR -c $APP_CONF 2>> /dev/null >> /dev/null &
            echo $! > $APP_PID
            echo "$APP_NAME started!"
        else
            echo "$APP_NAME is already running ..."
        fi
    ;;

    stop)
        if [ -f $APP_PID ]; then
            PID=$(cat $APP_PID);
            echo "Stopping $APP_NAME..."
            kill $PID;
            echo "$APP_NAME stopped!"
            rm $APP_PID
        else
            echo "$APP_NAME is not running ..."
        fi
    ;;

    status)
        if [ !  -f $APP_PID ]; then
            echo "$APP_NAME is stop"
        else
            echo "$APP_NAME is start"
        fi
        sleep 20s
    ;;

    version)
      java.exe -Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager -Dlog4j.configurationFile=file:$APP_LOG -jar $APP_JAR -version
      sleep 30s
    ;;

    *)
        echo "Choose an option start/stop/status/version for the service"
        sleep 20s
    ;;
esac