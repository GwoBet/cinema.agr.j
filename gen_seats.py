
def write4Hall(file, hallId, cinemaId, sessionId):
    fileDelimiter = ';'
    try:
        for row in range(3):
            for seat in range(row + 3):
                toWrite = str(row) + fileDelimiter + str(seat) + fileDelimiter + "0" + fileDelimiter + sessionId + hallId + cinemaId + '\n'
                file.write(toWrite)
    except Exception as e:
        file.close()
        print('Failed to write into file')
        print(e)
        raise Exception("Failed to write")

try:
    file = open("C:\Work\cinema.agr.j\\testStorages\Seats.txt", "w")
    write4Hall(file, 'CH001;', 'C001', 'SES0001;')
    write4Hall(file, 'CH001;', 'C001', 'SES0003;')
    write4Hall(file, 'CH001;', 'C001', 'SES0007;')
    write4Hall(file, 'CH002;', 'C001', 'SES0002;')
    write4Hall(file, 'CH002;', 'C001', 'SES0004;')
    write4Hall(file, 'CH003;', 'C002', 'SES0005;')
    write4Hall(file, 'CH004;', 'C002', 'SES0006;')
    file.close()
except Exception as err:
    print('Main error')
    print(err)

