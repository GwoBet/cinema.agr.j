package com.test.my;

import lombok.extern.log4j.Log4j2;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

@Log4j2
public class Test_getFilmList extends Common {

    @Test
    public void request() throws Exception{
        new Thread(() -> {
            Thread.currentThread().setName("test server");
            try {
                Main.main(new String[]{"--config=./config.xml"});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        //=================================== Success Request ===================================
        Response response = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/film/C001")
                .request()
                .get();
        if(response.getStatus() != 200) {
            throw new IllegalStateException(response.readEntity(String.class));
        }
        log.debug("status: " + response.getStatus());
        log.debug("Entity: " + response.readEntity(String.class));

        Response response2 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/film/detail/FM002").queryParam("cinemaId", "C001")
                .request()
                .get();
        if(response2.getStatus() != 200) {
            throw new IllegalStateException(response2.readEntity(String.class));
        }
        log.debug("status: " + response2.getStatus());
        log.debug("Entity: " + response2.readEntity(String.class));

        //=================================== Empty Query Request ===================================
        Response emptyResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/film")
                .request()
                .get();

        if(emptyResponse.getStatus() != 404) {
            throw new IllegalStateException(emptyResponse.readEntity(String.class));
        }
        log.debug("status: " + emptyResponse.getStatus());
        log.debug("Entity: " + emptyResponse.readEntity(String.class));

        Response emptyResponse2 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/film/detail/FM002")
                .request()
                .get();

        if(emptyResponse2.getStatus() != 400) {
            throw new IllegalStateException(emptyResponse2.readEntity(String.class));
        }
        log.debug("status: " + emptyResponse2.getStatus());
        log.debug("Entity: " + emptyResponse2.readEntity(String.class));

        //=================================== Wrong Query Request ===================================
        Response wrongResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/film/C010")
                .request()
                .get();

        if(wrongResponse.getStatus() != 200) {
            throw new IllegalStateException(wrongResponse.readEntity(String.class));
        }
        log.debug("status: " + wrongResponse.getStatus());
        log.debug("Entity: " + wrongResponse.readEntity(String.class));

        Response wrongResponse2 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/film/detail/FM004").queryParam("cinemaId", "C001")
                .request()
                .get();

        if(wrongResponse2.getStatus() != 404) {
            throw new IllegalStateException(wrongResponse2.readEntity(String.class));
        }
        log.debug("status: " + wrongResponse2.getStatus());
        log.debug("Entity: " + wrongResponse2.readEntity(String.class));
    }
}
