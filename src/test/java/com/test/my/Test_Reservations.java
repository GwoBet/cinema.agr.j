package com.test.my;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.my.srv.model.*;
import com.test.my.utils.dbutils.plug.dbplug.SeatsTable;
import lombok.extern.log4j.Log4j2;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class Test_Reservations extends Common {

    @Test
    public void request() throws Exception{
        new Thread(() -> {
            Thread.currentThread().setName("test server");
            try {
                Main.main(new String[]{"--config=./config.xml"});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        //=================================== Success Request ===================================
        //------------------- Success Block Request -------------------
        ReservationPointRequest request = new ReservationPointRequest();
        request.setCinemaId("C001");
        request.setSessionId("SES0003");
        List<ReservationRequestSeats> requestSeatsList = new ArrayList<>();
        ReservationRequestSeats requestSeats = new ReservationRequestSeats();
        requestSeats.setRow(2);
        List<ReservationRequestSeatsInRow> requestSeatsInRows = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            ReservationRequestSeatsInRow seatsInRow = new ReservationRequestSeatsInRow();
            seatsInRow.setSeat(i);
            requestSeatsInRows.add(seatsInRow);
        }
        requestSeats.setSeatsInRow(requestSeatsInRows);
        requestSeatsList.add(requestSeats);
        request.setSeats(requestSeatsList);
        Response pointResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/point")
                .request()
                .put(Entity.json(request));
        if(pointResponse.getStatus() != 204) {
            throw new IllegalStateException(pointResponse.readEntity(String.class));
        }
        log.debug("status: " + pointResponse.getStatus());
        log.debug("Entity: " + pointResponse.readEntity(String.class));

        //------------------- Success Reservation Request -------------------
        ReservationRequest reservationRequest = new ReservationRequest();
        reservationRequest.setSessionId("SES0003");
        reservationRequest.setCinemaId("C001");
        reservationRequest.setEmail("alex.sichyov@gmail.com");
        reservationRequest.setSeats(requestSeatsList);
        Response reserveResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/seat")
                .request()
                .post(Entity.json(reservationRequest));
        if(reserveResponse.getStatus() != 200) {
            throw new IllegalStateException(reserveResponse.readEntity(String.class));
        }

        log.debug("status: " + reserveResponse.getStatus());
        log.debug("Entity: " + reserveResponse.readEntity(String.class));


        //------------------- Success Refuse Request -------------------
        ReservationRefuseRequest refuseRequest = new ReservationRefuseRequest();
        refuseRequest.setCinemaId("C001");
        refuseRequest.setReserveId(SeatsTable.getReservationId("SES0003", 2, 1));
        refuseRequest.setSessionId("SES0003");
        Response refuseResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/refuse")
                .request()
                .put(Entity.json(refuseRequest));
        if(refuseResponse.getStatus() != 204) {
            throw new IllegalStateException(refuseResponse.readEntity(String.class));
        }
        log.debug("status: " + refuseResponse.getStatus());
        log.debug("Entity: " + refuseResponse.readEntity(String.class));

        //=================================== Empty Query Request ===================================
        //------------------- Empty Block Request -------------------
        ReservationPointRequest request2 = new ReservationPointRequest();
        Response pointResponse2 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/point")
                .request()
                .put(Entity.json(request2));
        if(pointResponse2.getStatus() != 500) {
            throw new IllegalStateException(pointResponse2.readEntity(String.class));
        }
        log.debug("status: " + pointResponse2.getStatus());
        log.debug("Entity: " + pointResponse2.readEntity(String.class));

        //------------------- Empty Reservation Request -------------------
        ReservationRequest reservationRequest2 = new ReservationRequest();
        Response reserveResponse2 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/seat")
                .request()
                .post(Entity.json(reservationRequest2));
        if(reserveResponse2.getStatus() != 500) {
            throw new IllegalStateException(reserveResponse2.readEntity(String.class));
        }
        log.debug("status: " + reserveResponse2.getStatus());
        log.debug("Entity: " + reserveResponse2.readEntity(String.class));

        //------------------- Empty Refuse Request -------------------
        ReservationRefuseRequest refuseRequest2 = new ReservationRefuseRequest();
        Response refuseResponse2 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/refuse")
                .request()
                .put(Entity.json(refuseRequest2));
        if(refuseResponse2.getStatus() != 500) {
            throw new IllegalStateException(refuseResponse2.readEntity(String.class));
        }
        log.debug("status: " + refuseResponse2.getStatus());
        log.debug("Entity: " + refuseResponse2.readEntity(String.class));

        //=================================== Wrong Query Request ===================================
        //------------------- Wrong Block Request -------------------
        ReservationPointRequest request3 = new ReservationPointRequest();
        request3.setCinemaId("C006");
        request3.setSessionId("SES0003");
        List<ReservationRequestSeats> requestSeatsList3 = new ArrayList<>();
        ReservationRequestSeats requestSeats3 = new ReservationRequestSeats();
        requestSeats3.setRow(2);
        List<ReservationRequestSeatsInRow> requestSeatsInRows3 = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            ReservationRequestSeatsInRow seatsInRow = new ReservationRequestSeatsInRow();
            seatsInRow.setSeat(i);
            requestSeatsInRows3.add(seatsInRow);
        }
        requestSeats3.setSeatsInRow(requestSeatsInRows3);
        requestSeatsList3.add(requestSeats3);
        request3.setSeats(requestSeatsList3);
        Response pointResponse3 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/point")
                .request()
                .put(Entity.json(request3));
        if(pointResponse3.getStatus() != 500) {
            throw new IllegalStateException(pointResponse3.readEntity(String.class));
        }
        log.debug("status: " + pointResponse3.getStatus());
        log.debug("Entity: " + pointResponse3.readEntity(String.class));

        //------------------- Wrong Reservation Request -------------------
        ReservationRequest reservationRequest3 = new ReservationRequest();
        reservationRequest3.setSessionId("SES0003");
        reservationRequest3.setCinemaId("C006");
        reservationRequest3.setEmail("alex.sichyov@gmail.com");
        reservationRequest3.setSeats(requestSeatsList3);
        Response reserveResponse3 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/seat")
                .request()
                .post(Entity.json(reservationRequest3));
        if(reserveResponse3.getStatus() != 500) {
            throw new IllegalStateException(reserveResponse3.readEntity(String.class));
        }
        log.debug("status: " + reserveResponse3.getStatus());
        log.debug("Entity: " + reserveResponse3.readEntity(String.class));

        //------------------- Wrong Refuse Request -------------------
        ReservationRefuseRequest refuseRequest3 = new ReservationRefuseRequest();
        refuseRequest3.setCinemaId("C006");
        refuseRequest3.setReserveId(SeatsTable.getReservationId("SES0003", 2, 1));
        refuseRequest3.setSessionId("SES0003");
        Response refuseResponse3 = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/refuse")
                .request()
                .put(Entity.json(refuseRequest3));
        if(refuseResponse3.getStatus() != 500) {
            throw new IllegalStateException(refuseResponse3.readEntity(String.class));
        }
        log.debug("status: " + refuseResponse3.getStatus());
        log.debug("Entity: " + refuseResponse3.readEntity(String.class));

    }


}
