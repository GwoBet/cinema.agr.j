package com.test.my;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.my.conf.Config;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.plug.dbplug.CinemaHallTable;
import com.test.my.utils.dbutils.plug.dbplug.CinemaTable;
import com.test.my.utils.dbutils.plug.dbplug.FilmTable;
import com.test.my.utils.dbutils.plug.dbplug.SeatsTable;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public class Common {


    protected  <T> T mapToClass(String json, Class<T> toClass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(json, toClass);
    }
}
