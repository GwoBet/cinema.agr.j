package com.test.my;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.my.srv.model.*;
import com.test.my.utils.dbutils.plug.dbplug.SeatsTable;
import lombok.extern.log4j.Log4j2;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.Test;

import javax.json.Json;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class Test_Script extends Common{

    private static final String CINEMA_NAME = "Победа";
    private static final String FILM_NAME = "Nobody";
    private static final String HALL_NAME = "Hall 1";

    @Test
    public void script() throws Exception{
        new Thread(() -> {
            Thread.currentThread().setName("test server");
            try {
                Main.main(new String[]{"--config=./config.xml"});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        //=================================== Success Cinema List Request ===================================
        Response cinemaListResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/cinema/all_info").queryParam("city", "Nsk")
                .request()
                .get();
        if(cinemaListResponse.getStatus() != 200) {
            throw new IllegalStateException(cinemaListResponse.readEntity(String.class));
        }
        String jsonCinemas = cinemaListResponse.readEntity(String.class);
        log.debug("status: " + cinemaListResponse.getStatus());
        log.debug("Entity: " + jsonCinemas);

        CinemaResponse cinemaResponse = mapToClass(jsonCinemas, CinemaResponse.class);
        //=================================== Success Film List Request ===================================

        String cinemaId = "";
        for (CinemaInfo cinemaInfo : cinemaResponse.getCinemas()){
            if (cinemaInfo.getCinemaName().equals(CINEMA_NAME)) {
                cinemaId = cinemaInfo.getCinemaId();
                break;
            }
        }
        Response filmListResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/film/" + cinemaId)
                .request()
                .get();
        if(filmListResponse.getStatus() != 200) {
            throw new IllegalStateException(filmListResponse.readEntity(String.class));
        }
        String jsonFilms = filmListResponse.readEntity(String.class);
        log.debug("status: " + filmListResponse.getStatus());
        log.debug("Entity: " + jsonFilms);

        FilmListResult filmListResult = mapToClass(jsonFilms, FilmListResult.class);

        String filmId = "";
        for (FilmInfo filmInfo : filmListResult.getFilms()){
            if (filmInfo.getName().equals(FILM_NAME)) {
                filmId = filmInfo.getFilmId();
                break;
            }
        }

        //=================================== Success Film Detail Info Request ===================================

        Response filmDetailResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/film/detail/" + filmId).queryParam("cinemaId", cinemaId)
                .request()
                .get();
        if(filmDetailResponse.getStatus() != 200) {
            throw new IllegalStateException(filmDetailResponse.readEntity(String.class));
        }

        String jsonFilmDetail = filmDetailResponse.readEntity(String.class);
        log.debug("status: " + filmDetailResponse.getStatus());
        log.debug("Entity: " + jsonFilmDetail);

        FilmDetailInfo filmDetailsResult = mapToClass(jsonFilmDetail, FilmDetailInfo.class);

        String sessionId = "";
        for (FilmDetailInfoSessions sessionDetail : filmDetailsResult.getSessions()){
            if (sessionDetail.getHallName().equals(HALL_NAME)) {
                sessionId = sessionDetail.getSessionId();
                break;
            }
        }

        //=================================== Success Cinema Hall Request ===================================

        Response cinemaHallResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/cinema_hall/" + cinemaId).queryParam("filmId", filmId)
                .queryParam("sessionId", sessionId)
                .request()
                .get();
        if(cinemaHallResponse.getStatus() != 200) {
            throw new IllegalStateException(cinemaHallResponse.readEntity(String.class));
        }

        String jsonCinemaHall = cinemaHallResponse.readEntity(String.class);
        log.debug("status: " + cinemaHallResponse.getStatus());
        log.debug("Entity: " + jsonCinemaHall);

        //=================================== Success Block Request ===================================
        ReservationPointRequest request = new ReservationPointRequest();
        request.setCinemaId(cinemaId);
        request.setSessionId(sessionId);
        List<ReservationRequestSeats> requestSeatsList = new ArrayList<>();
        ReservationRequestSeats requestSeats = new ReservationRequestSeats();
        requestSeats.setRow(2);
        List<ReservationRequestSeatsInRow> requestSeatsInRows = new ArrayList<>();

        ReservationRequestSeatsInRow seatInRow = new ReservationRequestSeatsInRow();
        seatInRow.setSeat(2);
        requestSeatsInRows.add(seatInRow);
        ReservationRequestSeatsInRow seat2InRow = new ReservationRequestSeatsInRow();
        seat2InRow.setSeat(1);
        requestSeatsInRows.add(seat2InRow);

        requestSeats.setSeatsInRow(requestSeatsInRows);
        requestSeatsList.add(requestSeats);
        request.setSeats(requestSeatsList);
        Response pointResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/point")
                .request()
                .put(Entity.json(request));
        if(pointResponse.getStatus() != 204) {
            throw new IllegalStateException(pointResponse.readEntity(String.class));
        }

        log.debug("status: " + pointResponse.getStatus());
        log.debug("Entity: " + pointResponse.readEntity(String.class));

        //=================================== Success Reservation Request ===================================
        ReservationRequest reservationRequest = new ReservationRequest();
        reservationRequest.setSessionId(sessionId);
        reservationRequest.setCinemaId(cinemaId);
        reservationRequest.setEmail("alex.sichyov@gmail.com");
        reservationRequest.setSeats(requestSeatsList);
        Response reserveResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/seat")
                .request()
                .post(Entity.json(reservationRequest));
        if(reserveResponse.getStatus() != 200) {
            throw new IllegalStateException(reserveResponse.readEntity(String.class));
        }

        String jsonResevation = reserveResponse.readEntity(String.class);
        log.debug("status: " + reserveResponse.getStatus());
        log.debug("Entity: " + jsonResevation);

        ReservationResponse reservationResponse = mapToClass(jsonResevation, ReservationResponse.class);

        //=================================== Success Cinema Hall Request ===================================

        Response cinemaHallResponseAfter = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/cinema_hall/" + cinemaId).queryParam("filmId", filmId)
                .queryParam("sessionId", sessionId)
                .request()
                .get();
        if(cinemaHallResponseAfter.getStatus() != 200) {
            throw new IllegalStateException(cinemaHallResponseAfter.readEntity(String.class));
        }

        String jsonCinemaHall2 = cinemaHallResponseAfter.readEntity(String.class);
        log.debug("status: " + cinemaHallResponseAfter.getStatus());
        log.debug("Entity: " + jsonCinemaHall2);

        //=================================== Success Refuse Request ===================================
        ReservationRefuseRequest refuseRequest = new ReservationRefuseRequest();
        refuseRequest.setCinemaId(cinemaId);
        refuseRequest.setReserveId(reservationResponse.getReserveId());
        refuseRequest.setSessionId(sessionId);
        Response refuseResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/reserve/refuse")
                .request()
                .put(Entity.json(refuseRequest));
        if(refuseResponse.getStatus() != 204) {
            throw new IllegalStateException(refuseResponse.readEntity(String.class));
        }
        log.debug("status: " + refuseResponse.getStatus());
        log.debug("Entity: " + refuseResponse.readEntity(String.class));

        //=================================== Success Cinema Hall Request ===================================

        Response cinemaHallResponseLast = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/cinema_hall/" + cinemaId).queryParam("filmId", filmId)
                .queryParam("sessionId", sessionId)
                .request()
                .get();
        if(cinemaHallResponseLast.getStatus() != 200) {
            throw new IllegalStateException(cinemaHallResponseLast.readEntity(String.class));
        }

        String jsonCinemaHall3 = cinemaHallResponseLast.readEntity(String.class);
        log.debug("status: " + cinemaHallResponseLast.getStatus());
        log.debug("Entity: " + jsonCinemaHall3);

    }

}
