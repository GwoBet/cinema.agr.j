package com.test.my;

import lombok.extern.log4j.Log4j2;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

@Log4j2
public class Test_getCinemaHall extends Common {

    @Test
    public void request() throws Exception{

        new Thread(() -> {
            Thread.currentThread().setName("test server");
            try {
                Main.main(new String[]{"--config=./config.xml"});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        //=================================== Success Request ===================================
        Response response = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/cinema_hall/C001").queryParam("filmId", "FM001")
                .queryParam("sessionId", "SES0002")
                .request()
                .get();
        if(response.getStatus() != 200) {
            throw new IllegalStateException(response.readEntity(String.class));
        }
        log.debug("status: " + response.getStatus());
        log.debug("Entity: " + response.readEntity(String.class));

        //=================================== Empty Query Request ===================================
        Response emptyResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/cinema_hall")
                .request()
                .get();

        if(emptyResponse.getStatus() != 404) {
            throw new IllegalStateException(emptyResponse.readEntity(String.class));
        }
        log.debug("status: " + emptyResponse.getStatus());
        log.debug("Entity: " + emptyResponse.readEntity(String.class));

        //=================================== Wrong Query Request ===================================
        Response wrongResponse = ClientBuilder.newClient(new ClientConfig()
                .property(ClientProperties.READ_TIMEOUT, 30000)
                .property(ClientProperties.CONNECT_TIMEOUT, 5000))
                .target("http://localhost:8080").path("/cinema_hall/C004").queryParam("filmId", "FM004")
                .request()
                .get();

        if(wrongResponse.getStatus() != 400) {
            throw new IllegalStateException(wrongResponse.readEntity(String.class));
        }
        log.debug("status: " + wrongResponse.getStatus());
        log.debug("Entity: " + wrongResponse.readEntity(String.class));

    }
}
