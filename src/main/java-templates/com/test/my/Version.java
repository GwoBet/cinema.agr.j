package com.test.my;

public class Version {
    public static final String version = " Version: [${project.Version}], Branch: [${git-branch-id}], Commit: [${git-commit-id}], Build timestamp: [${git-build-time}]";

    @Override
    public String toString() {
        return version;
    }
}