package com.test.my;

import lombok.extern.log4j.Log4j2;
import org.glassfish.jersey.message.internal.ReaderWriter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.*;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Log4j2
@PreMatching
@Provider
public class LogTraceFilter implements ContainerRequestFilter, ContainerResponseFilter, WriterInterceptor {

  @Override
  public void filter(ContainerRequestContext requestContext) {
    byte[] body = new byte[0];
    if (log.isTraceEnabled()) {
      try {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = requestContext.getEntityStream();
        ReaderWriter.writeTo(in, out);
        body = out.toByteArray();
        requestContext.setEntityStream(new ByteArrayInputStream(body));
      } catch (Exception ex) {
        log.error("EXCEPTION "+ex.getMessage(), ex);
      }
    }
    log.trace("------------- start connection ------------\n\theaders:" +
            requestContext.getHeaders().toString() +
            "\n\turi:" + requestContext.getUriInfo().getAbsolutePath().toString() +
            "\tquery:" + requestContext.getUriInfo().getRequestUri().getRawQuery() +
            "\tmethod:" + requestContext.getMethod() +
            "\n\tbody:" + new String(body, StandardCharsets.UTF_8));

  }

  @Override
  public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
    if (responseContext.getStatus() == 404 || responseContext.getStatus() == 415) {
      String builder = ("------------- 404 or 415 status ------------\n\theaders:" + requestContext.getHeaders().toString()) +
              "\n\turi:" + requestContext.getUriInfo().getAbsolutePath().toString() +
              "\tquery:" + requestContext.getUriInfo().getRequestUri().getRawQuery() +
              "\tmethod:" + requestContext.getMethod();
      log.trace(builder);
    }
    log.trace("------------- response status:" + responseContext.getStatus() + "\theaders:" + responseContext.getHeaders());
  }

  @Override
  public void aroundWriteTo(WriterInterceptorContext writerInterceptorContext) throws IOException, WebApplicationException {
    OutputStream out = writerInterceptorContext.getOutputStream();
    List<Object> hd = writerInterceptorContext.getHeaders().get("Content-Type");
    String content = hd != null && !hd.isEmpty() && hd.get(0) != null ? hd.get(0).toString() : "";
    try (ByteArrayOutputStream bout = new ByteArrayOutputStream()) {
      writerInterceptorContext.setOutputStream(bout);
      writerInterceptorContext.proceed();
      bout.writeTo(out);
      writerInterceptorContext.setOutputStream(out);
      log.trace("------------- close connection RESPONSE BODY[" + content + "]:" +
              bout.toString("UTF-8"));
    }
  }
}
