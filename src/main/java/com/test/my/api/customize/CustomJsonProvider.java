package com.test.my.api.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.test.my.conf.Config;
import com.test.my.utils.ConfigHelper;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class CustomJsonProvider extends JacksonJaxbJsonProvider {

  public CustomJsonProvider() {
    super();
    Config.Common common = ConfigHelper.getInstance().getCfg().getCommon();

    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(common.isJsonOutNull() ? JsonInclude.Include.ALWAYS : JsonInclude.Include.NON_NULL);
    if (common.isJsonOutIdent()) mapper.enable(SerializationFeature.INDENT_OUTPUT);
    setMapper(mapper);
  }
}