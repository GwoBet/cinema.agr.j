package com.test.my.api;

import com.test.my.Version;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Log4j2
@Path("/")
public class VersionApi {

    @GET
    @Path("/version")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getVersion(@QueryParam("db") String db) {
        //if (db != null) return getVersionFull();
        return Response.ok(Version.version).build();
    }
}