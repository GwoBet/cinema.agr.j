package com.test.my.api;

import lombok.extern.log4j.Log4j2;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Log4j2
@Provider
public class JSONExceptionMapper implements ExceptionMapper<com.fasterxml.jackson.core.JsonProcessingException> {

  public Response toResponse(final com.fasterxml.jackson.core.JsonProcessingException ex) {
    log.warn(ex.toString());
    return Response.status(Response.Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN_TYPE).entity("Invalid json data:" + ex.getMessage()).build();
  }
}
