package com.test.my.api;

import com.test.my.exceptions.AppException;
import lombok.extern.log4j.Log4j2;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Log4j2
@Provider
public class AllExceptionMapper implements ExceptionMapper<Exception> {

  public Response toResponse(final Exception ex) {
    log.warn(ex.toString(), ex);
    if (ex instanceof NotFoundException)
      return Response.status(404).type(MediaType.TEXT_PLAIN_TYPE + ";charset=utf-8").entity("Запрошенный сервис не существует").build();
    AppException e = new AppException(AppException.Code.UNDEFINED_ERROR, ex.getMessage());
    return Response.status(e.getHttpStatus()).type(MediaType.TEXT_PLAIN_TYPE + ";charset=utf-8").entity(e.getMessage()).build();
  }
}
