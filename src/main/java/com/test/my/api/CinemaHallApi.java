package com.test.my.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.CinemaHallResponse;
import com.test.my.utils.dbutils.plug.api.DbCinemaHallApi;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Log4j2
@Path("/")
public class CinemaHallApi {

    @SuppressWarnings("unused") @Inject
    private DbCinemaHallApi dbCinemaHallApi;

    @GET
    @Path("/cinema_hall/{cinemaId}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getCinemaHall(@NotNull @PathParam("cinemaId") String cinemaId,
                                @NotNull @QueryParam("filmId") String filmId,
                                @NotNull @QueryParam("sessionId") String sessionId) throws AppException {
        String func = "getCinemaHall";
        log.debug("++++++++++ " + func + ":  cinemaId=[ " + cinemaId + " ] filmId=[ " + filmId + "] sessionId=[ " + sessionId + "] ");
        CinemaHallResponse response = dbCinemaHallApi.getCinemaHall(cinemaId, filmId, sessionId);
        log.debug("---------- " + func + ": " + response.toString());
        return Response.ok(response).build();
    }
}
