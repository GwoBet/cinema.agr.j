package com.test.my.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.FilmDetailInfo;
import com.test.my.srv.model.FilmListResult;
import com.test.my.utils.dbutils.plug.api.DbFilmApi;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Log4j2
@Path("/")
public class FilmApi {

    @SuppressWarnings("unused") @Inject private DbFilmApi dbFilmApi;

    @GET
    @Path("/film/{cinemaId}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getFilmList(@NotNull @PathParam("cinemaId") String cinemaId) throws AppException {
        String func = "getFilmList";
        log.debug("++++++++++ " + func + ": cinemaId=[ " + cinemaId + " ]");
        FilmListResult result = dbFilmApi.getFilmList(cinemaId);
        log.debug("---------- " + func + ": " + result.toString());
        return Response.ok(result).build();
    }

    @GET
    @Path("/film/detail/{filmId}")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getFilmDetailInfo(@NotNull @PathParam("filmId") String filmId,
                                      @NotNull @QueryParam("cinemaId") String cinemaId) throws AppException {
        String func = "getFilmDetailInfo";
        log.debug("++++++++++ " + func + ": filmId=[ " + filmId + " ] cinemaId=[ " + cinemaId + "]");
        FilmDetailInfo result = dbFilmApi.getFilmDetailInfo(filmId, cinemaId);
        log.debug("---------- " + func + ": " + result.toString());
        return Response.ok(result).build();
    }
}
