package com.test.my.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.ReservationPointRequest;
import com.test.my.srv.model.ReservationRefuseRequest;
import com.test.my.srv.model.ReservationRequest;
import com.test.my.srv.model.ReservationResponse;
import com.test.my.utils.dbutils.plug.api.DbReservationApi;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.zip.CRC32;

@Log4j2
@Path("/")
public class ReservationApi {

    @SuppressWarnings("unused") @Inject
    private DbReservationApi dbReservationApi;

    @PUT
    @Path("/reserve/point")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response makeReservePoint(ReservationPointRequest rq) throws AppException {
        String func = "makeReservePoint";
        log.debug("++++++++++ " + func + ": " + rq.toString());
        dbReservationApi.pointReserve(rq);
        log.debug("---------- " + func + " ----------");
        return Response.noContent().build();
    }


    @POST
    @Path("/reserve/seat")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response makeReserve(ReservationRequest rq) throws AppException {
        String func = "getReserveEmail";
        log.debug("++++++++++ " + func + ": " + rq.toString());

        ReservationResponse reservationResponse = new ReservationResponse();

        String reservationNumber = makeReservationNumber();
        reservationResponse.setReserveId(reservationNumber);

        dbReservationApi.makeReserve(rq, reservationNumber);

        //TODO: make seat reservation: Send Email with ReservationNumber
        log.debug("---------- " + func + " ----------");
        return Response.ok(reservationResponse).build();
    }

    @PUT
    @Path("/reserve/refuse")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response makeReserveRefuse(ReservationRefuseRequest rq) throws AppException {
        String func = "getReserveEmail";
        log.debug("++++++++++ " + func + ": " + rq.toString());
        dbReservationApi.refuseReserve(rq);
        log.debug("---------- " + func + " ----------");
        return Response.noContent().build();
    }


    private String makeReservationNumber (){
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String strDate = dateFormat.format(date);
        CRC32 crc = new CRC32();
        crc.update(strDate.getBytes());
        return "RES" + Long.toHexString(crc.getValue());
    }
}
