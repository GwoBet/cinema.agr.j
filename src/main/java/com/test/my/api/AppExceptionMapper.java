package com.test.my.api;

import com.test.my.exceptions.AppException;
import lombok.extern.log4j.Log4j2;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Log4j2
@Provider
public class AppExceptionMapper implements ExceptionMapper<AppException> {

  public Response toResponse(final AppException e) {
    log.trace(e.toString(), e);
    return e.toResponse();
  }
}
