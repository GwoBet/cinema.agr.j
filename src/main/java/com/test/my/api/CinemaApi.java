package com.test.my.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.CinemaResponse;
import com.test.my.utils.dbutils.plug.api.DbCinemaApi;
import lombok.extern.log4j.Log4j2;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Log4j2
@Path("/")
public class CinemaApi {

    @SuppressWarnings("unused") @Inject private DbCinemaApi dbCinemaApi;

    @GET
    @Path("/cinema/all_info")
    @Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
    public Response getCinemaList(@NotNull @QueryParam("city") String cityFilter,
                                  @QueryParam("street") String street) throws AppException {
        String func = "getCinemaList";
        log.debug("++++++++++ " + func + ": cityFilter=[ " + cityFilter + " ] street=[ " + street + "]");
        CinemaResponse response = dbCinemaApi.getCinemaList(cityFilter, street);
        log.debug("---------- " + func + ": " + response.toString());
        return Response.ok(response).build();
    }
}
