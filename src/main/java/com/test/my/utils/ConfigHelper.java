package com.test.my.utils;

import com.test.my.conf.Config;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Log4j2
public class ConfigHelper {

  private String configFileName;
  private static long lastModified;
  @Getter
  Config cfg;
  @Getter static ConfigHelper instance = null;


  private ConfigHelper(String configFileName) throws JAXBException {
    cfg = loadConfig(configFileName);
    this.configFileName = configFileName;
  }

  private static Config loadConfig(String configFileName) throws JAXBException {
    File fcfg = new File(configFileName);
    if (!fcfg.exists()) throw new IllegalArgumentException("config file not found [" + fcfg + "]");
    lastModified = fcfg.lastModified();
    // Загрузка конфига
    javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(Config.class);
    return (Config) jaxbContext.createUnmarshaller().unmarshal(fcfg);
  }

  public static ConfigHelper setInstance(String configFileName) throws JAXBException {
    instance = new ConfigHelper(configFileName);
    return instance;
  }

  public static void startReLoader(int reloadConfigPeriod) {
    log.trace("reloadConfigPeriod:" + reloadConfigPeriod);
    if (reloadConfigPeriod != 0) {
      Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() -> {
        Thread.currentThread().setName("CONFIG RELOAD");
        File cf = new File(instance.configFileName);
        if (cf.exists() && lastModified < cf.lastModified()) {
          log.info("Reload config file:" + instance.configFileName);
          try {
            setInstance(instance.configFileName);
          } catch (Exception e) {
            log.error("Reload config file. " + e.getMessage(), e);
          }
        }
      }, reloadConfigPeriod, reloadConfigPeriod, TimeUnit.SECONDS);
    }
  }

}
