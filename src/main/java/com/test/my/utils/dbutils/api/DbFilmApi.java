package com.test.my.utils.dbutils.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.*;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.ProxyCallableDB;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DbFilmApi {

    private ResultSet resultSet;
    private String cinemaId;
    private String filmId;
    public static final String GET_FILM_LIST = "SELECT * " +
                                                "FROM film_table " +
                                                "WHERE cinemaId = :p$cinemaId ;";
    public static final String GET_FILM_DETAIL_INFO = "SELECT * " +
                                                        "FROM film_table " +
                                                        "WHERE cinemaId = :p$cinemaId AND filmId = :p$filmId ;";
    public static final String GET_SESSIONS_INFO = "SELECT sessionId, filmTime, hallName, hallId " +
                                                    "FROM cinema_hall_table " +
                                                    "WHERE filmId = :p$filmId AND cinemaId = :p$cinemaId ;";

    public FilmListResult getFilmList(String cinemaId) throws AppException {
        this.cinemaId = cinemaId;
        try{
            executeDBForList();

            return mapResultSet();
        }catch (SQLException e){
            log.error("Failed to get Film Data: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Film Data: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]");
        }catch (Exception e){
            log.error("Failed to get Film Data: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Film Data: msg=[ " + e.getMessage() + "]");
        }
    }

    private void executeDBForList() throws SQLException {
        try(ProxyCallableDB db = new ProxyCallableDB()){
            String readyExec = GET_FILM_LIST;
            db.prepare(readyExec);
            db.setString(":p$cinemaId", cinemaId);

            resultSet = db.executeStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout());
        }
    }

    private FilmListResult mapResultSet() throws SQLException, AppException {
        FilmListResult filmListResult = new FilmListResult();
        try{
            Class<FilmInfo> filmInfoClass = FilmInfo.class;
            Field[] fields = filmInfoClass.getDeclaredFields();

            List<FilmInfo> filmInfoList = new ArrayList<>();
            while (resultSet.next()) {
                FilmInfo filmInfo = new FilmInfo();

                for (Field field : fields) {
                    String name = field.getName();

                    String value = resultSet.getString(name);
                    field.set(filmInfo, field.getType().getConstructor(String.class).newInstance(value));
                }
                filmInfoList.add(filmInfo);
            }
            filmListResult.setFilms(filmInfoList);
        }catch ( NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e){
            log.error("DbFilmApi::mapResultSet: " + e.getClass() + " message: " + e.getMessage(), e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "DbFilmApi::mapResultSet: " + e.getClass() + " message: " + e.getMessage());
        }
        return filmListResult;
    }

    //================================= DETAIL INFORMATION =================================

    public FilmDetailInfo getFilmDetailInfo(String filmId, String cinemaId) throws AppException {
        this.filmId = filmId;
        this.cinemaId = cinemaId;
        try{
            executeDBForDetailInfo();

            return mapResultSetForDetailInfo();
        }catch (SQLException e){
            log.error("Failed to get Film Info Data: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Film Info Data: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]");
        }catch (Exception e){
            log.error("Failed to get Film Info Data: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Film Info Data: msg=[ " + e.getMessage() + "]");
        }
    }

    private void executeDBForDetailInfo() throws SQLException {
        try(ProxyCallableDB db = new ProxyCallableDB()){
            String readyExec = GET_FILM_DETAIL_INFO;
            db.prepare(readyExec);
            db.setString(":p$cinemaId", cinemaId);
            db.setString(":p$filmId", filmId);

            resultSet = db.executeStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout());
        }
    }

    private void executeDBForSessions() throws SQLException {
        try(ProxyCallableDB db = new ProxyCallableDB()){
            String readyExec = GET_SESSIONS_INFO;
            db.prepare(readyExec);
            db.setString(":p$filmId", filmId);
            db.setString(":p$cinemaId", cinemaId);

            resultSet = db.executeStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout());
        }
    }

    private FilmDetailInfo mapResultSetForDetailInfo() throws SQLException, AppException {
        FilmDetailInfo filmDetailInfo = new FilmDetailInfo();
        try{
            Class<FilmDetailInfo> filmDetailInfoClass = FilmDetailInfo.class;
            Field[] fields = filmDetailInfoClass.getDeclaredFields();

            Class<FilmDetailInfoSessions> filmDetailInfoSessionsClass = FilmDetailInfoSessions.class;
            Field[] sessionFields = filmDetailInfoSessionsClass.getDeclaredFields();

            List<FilmDetailInfoSessions> filmDetailInfoSessionsList = new ArrayList<>();

            while (resultSet.next()) {
                for (Field field : fields) {
                    if (!field.getType().equals(List.class)) {
                        String name = field.getName();

                        String value = resultSet.getString(name);
                        field.set(filmDetailInfo, field.getType().getConstructor(String.class).newInstance(value));
                    }
                }
            }

            //=================== Get Sessions List ===================
            executeDBForSessions();

            while (resultSet.next()) {
                FilmDetailInfoSessions filmDetailInfoSession = new FilmDetailInfoSessions();

                for (Field field : sessionFields) {
                    String name = field.getName();

                    String value = resultSet.getString(name);
                    field.set(filmDetailInfoSession, field.getType().getConstructor(String.class).newInstance(value));
                }
                filmDetailInfoSessionsList.add(filmDetailInfoSession);
            }
            filmDetailInfo.setSessions(filmDetailInfoSessionsList);

        }catch ( NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e){
            log.error("DbFilmApi::mapResultSet: " + e.getClass() + " message: " + e.getMessage(), e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "DbFilmApi::mapResultSet: " + e.getClass() + " message: " + e.getMessage());
        }
        return filmDetailInfo;
    }

}
