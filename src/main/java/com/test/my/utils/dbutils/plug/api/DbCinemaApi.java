package com.test.my.utils.dbutils.plug.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.CinemaInfo;
import com.test.my.srv.model.CinemaResponse;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.ProxyCallableDB;
import com.test.my.utils.dbutils.plug.dbplug.CinemaTable;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DbCinemaApi {

    public CinemaResponse getCinemaList(String cityFilter, String street) throws AppException {
        try{
            CinemaResponse cinemaResponse = execute(cityFilter, street == null ? "" : street);

            return cinemaResponse;
        }catch (Exception e){
            log.error("Failed to get Cinema Data: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Cinema Data: msg=[ " + e.getMessage() + "]");
        }
    }

    private CinemaResponse execute(String cityFilter, String street) {
        List<CinemaInfo> cinemaInfoList = new ArrayList<>();
        if (street.equals(""))
            cinemaInfoList = CinemaTable.get(cityFilter);
        else
            cinemaInfoList = CinemaTable.get(cityFilter, street);
        CinemaResponse response = new CinemaResponse();
        response.setCinemas(cinemaInfoList);
        return response;
    }

}
