package com.test.my.utils.dbutils.plug.models;

import lombok.Data;

@Data
public class SeatsModel {

    private int row;

    private int seat;

    private int reserved;

    private String sessionId;

    private String hallId;

    private String cinemaId;

    private String reservationId;
}
