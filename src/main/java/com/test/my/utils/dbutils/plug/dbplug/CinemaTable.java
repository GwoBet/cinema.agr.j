package com.test.my.utils.dbutils.plug.dbplug;

import com.test.my.exceptions.AppException;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;
import com.test.my.srv.model.CinemaInfo;
import lombok.extern.log4j.Log4j2;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Log4j2
public class CinemaTable {

    private static String cinemaFile;
    private static String fileDelimiter = ";";


    AtomicInteger counter = new AtomicInteger(0);

    private static Map<String, CinemaInfo> cinemaMap = new ConcurrentHashMap<>();

    public static void loadStorage(String filePath) throws PlugException {
        if(!new File(filePath).exists()) {
            log.trace("no Cinema file: "+filePath);
            return;
        }

        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(s->{
                String []items = s.split(fileDelimiter);
                CinemaInfo cinemaInfo = new CinemaInfo();
                cinemaInfo.setCinemaName(items[1]);
                cinemaInfo.setCinemaId(items[0]);
                cinemaInfo.setWorkTime(items[2]);
                cinemaInfo.setCity(items[3]);
                cinemaInfo.setStreet(items[4]);
                cinemaInfo.setBuilding(items[5]);
                cinemaMap.put(items[0], cinemaInfo);
            });
        } catch (IOException e) {
            log.error("Can't load file '" + cinemaFile + "'. " + e.getMessage(), e);
            throw new PlugException("Can't load file '" + cinemaFile + "'. " + e.getMessage());
        }
    }

    public static void delete(String cinemaId){
        cinemaMap.remove(cinemaId);
    }

    public static List<CinemaInfo> get(String city,
                      String street){
        List<CinemaInfo> cinemaInfoList = new ArrayList<>();
        CinemaInfo result = new CinemaInfo();
        for (String id : cinemaMap.keySet()){
            result = cinemaMap.get(id);
            if (result.getCity().equals(city) && result.getStreet().equals(street)){
                cinemaInfoList.add(result);
            }
        }
        return cinemaInfoList;
    }

    public static List<CinemaInfo> get(String city){
        List<CinemaInfo> cinemaInfoList = new ArrayList<>();
        CinemaInfo result = new CinemaInfo();
        for (String id : cinemaMap.keySet()){
            result = cinemaMap.get(id);
            if (result.getCity().equals(city)){
                cinemaInfoList.add(result);
            }
        }
        return cinemaInfoList;
    }
}
