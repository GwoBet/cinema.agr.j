package com.test.my.utils.dbutils.plug.dbplug;

import com.test.my.srv.model.SeatInfo;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;
import com.test.my.utils.dbutils.plug.models.SeatsModel;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
@Log4j2
public class SeatsTable {


    AtomicInteger counter = new AtomicInteger(0);
    private static String fileDelimiter = ";";

    private static List<SeatsModel> seatsList = Collections.synchronizedList(new ArrayList<>());

    public static void loadStorage(String filePath) throws PlugException {
        if(!new File(filePath).exists()) {
            log.trace("no Seats file: "+filePath);
            return;
        }

        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(s->{
                String []items = s.split(fileDelimiter);
                SeatsModel seatInfo = new SeatsModel();
                seatInfo.setRow(Integer.parseInt(items[0]));
                seatInfo.setSeat(Integer.parseInt(items[1]));
                seatInfo.setReserved(Integer.parseInt(items[2]));
                seatInfo.setSessionId(items[3]);
                seatInfo.setHallId(items[4]);
                seatInfo.setCinemaId(items[5]);
                seatInfo.setReservationId("");
                seatsList.add(seatInfo);
            });
        } catch (IOException e) {
            log.error("Can't load file '" + filePath + "'. " + e.getMessage(), e);
            throw new PlugException("Can't load file '" + filePath + "'. " + e.getMessage());
        }
    }

    public static String getReservationId(String sessionId,
                                          int row,
                                          int seat){
        String reservationId;
        Optional<SeatsModel> optionalSeat = seatsList.stream().filter(s -> (s.getRow() == row) && (s.getSeat() == seat) && (s.getSessionId().equals(sessionId))).findFirst();
        if (optionalSeat.isPresent())
            reservationId = optionalSeat.get().getReservationId();
        else
            reservationId = "";
        return reservationId;
    }

    public static List<SeatsModel> get(String cinemaId,
                                      String hallId,
                                      String sessionId) throws PlugException {
        try{
            List<SeatsModel> result = new ArrayList<>();

            result = seatsList.stream().filter(c -> c.getHallId().equals(hallId)
                   && c.getCinemaId().equals(cinemaId)
                   && c.getSessionId().equals(sessionId)).collect(Collectors.toList());

            return result;
        }catch (Exception e) {
            throw new PlugException("No Seats with cinemaId=[ " + cinemaId + " ] hallId=[ " + hallId + " ] sessionId=[ " + sessionId + " ]");
        }
    }

    public static List<SeatInfo> getSeats(String cinemaId,
                                          String hallId,
                                          String sessionId) throws PlugException {
        try{
            List<SeatInfo> result = new ArrayList<>();

            for(SeatsModel seatsModel : seatsList){
                if (seatsModel.getHallId().equals(hallId)
                        && seatsModel.getCinemaId().equals(cinemaId)
                        && seatsModel.getSessionId().equals(sessionId)){
                    SeatInfo seatInfo = new SeatInfo();
                    seatInfo.setSeat(seatsModel.getSeat());
                    seatInfo.setRow(seatsModel.getRow());
                    seatInfo.setReserved(seatsModel.getReserved() > 0);
                    result.add(seatInfo);
                }
            }
            return result;
        }catch (Exception e) {
            throw new PlugException("No Seats with cinemaId=[ " + cinemaId + " ] hallId=[ " + hallId + " ] sessionId=[ " + sessionId + " ]");
        }
    }

    public static int reserveSeats(String sessionId,
                                        String cinemaId,
                                        int row,
                                        String reservationId,
                                        List<Integer> seats) throws PlugException {
        AtomicInteger count = new AtomicInteger();
        try{

            seatsList.stream().filter(s ->
                    s.getSessionId().equals(sessionId)
                            && s.getCinemaId().equals(cinemaId)
                            && (s.getRow() == row) && seats.contains(s.getSeat())
            ).forEach(s -> {
                s.setReservationId(reservationId);
                count.getAndIncrement();
            });
        }catch (Exception e){
            log.error("Failed to reserve seats");
            throw new PlugException("Failed to reserve seats");
        }
        return count.get();
    }

    public static int lockReservedSeats(String sessionId,
                                        String cinemaId,
                                        int row,
                                        List<Integer> seats) throws PlugException {
        AtomicInteger count = new AtomicInteger();
        try{

             seatsList.stream().filter(s ->
                     s.getSessionId().equals(sessionId)
                             && s.getCinemaId().equals(cinemaId)
                             && (s.getRow() == row) && seats.contains(s.getSeat())
             ).forEach(s -> {
                 s.setReserved(1);
                 count.getAndIncrement();
             });

        }catch (Exception e){
            log.error("Failed to reserve seats");
            throw new PlugException("Failed to reserve seats");
        }
        return count.get();
    }

    public static int refuseReservedSeats(String sessionId,
                                          String cinemaId,
                                          String reservationId) throws PlugException {
        AtomicInteger count = new AtomicInteger();
        try{

            seatsList.stream().filter(s ->
                    s.getSessionId().equals(sessionId)
                            && s.getCinemaId().equals(cinemaId)
                            && s.getReservationId().equals(reservationId)
            ).forEach(s -> {
                s.setReserved(0);
                s.setReservationId("");
                count.getAndIncrement();
            });
        }catch (Exception e){
            log.error("Failed to refuse reserve seats", e);
            throw new PlugException("Failed to refuse reserve seats");
        }
        return count.get();
    }
}
