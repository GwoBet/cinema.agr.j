package com.test.my.utils.dbutils.plug.dbplug;


import com.test.my.srv.model.CinemaInfo;
import com.test.my.srv.model.FilmDetailInfoSessions;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;
import com.test.my.utils.dbutils.plug.models.CinemaHallModel;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Log4j2
public class CinemaHallTable {

    private static String fileDelimiter = ";";

    private static List<CinemaHallModel> cinemaHallList = Collections.synchronizedList(new ArrayList<>());

    public static void loadStorage(String filePath) throws PlugException {
        if(!new File(filePath).exists()) {
            log.trace("no Cinema file: " +filePath);
            return;
        }

        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(s->{
                String []items = s.split(fileDelimiter);
                CinemaHallModel cinemaInfo = new CinemaHallModel();
                cinemaInfo.setHallId(items[0]);
                cinemaInfo.setHallName(items[1]);
                cinemaInfo.setCinemaId(items[2]);
                cinemaInfo.setFilmId(items[3]);
                cinemaInfo.setFilmTime(items[4]);
                cinemaInfo.setSessionId(items[5]);

                cinemaHallList.add(cinemaInfo);
            });
        } catch (IOException e) {
            log.error("Can't load file '" + filePath + "'. " + e.getMessage(), e);
            throw new PlugException("Can't load file '" + filePath + "'. " + e.getMessage());
        }
    }

    public static void delete(String cinemaId){
        cinemaHallList.remove(cinemaId);
    }

    public static CinemaHallModel get(String cinemaId,
                          String filmId,
                          String sessionId) throws PlugException {
        for (CinemaHallModel result : cinemaHallList){
            if (result.getCinemaId().equals(cinemaId) && result.getFilmId().equals(filmId) && result.getSessionId().equals(sessionId)){
                return result;
            }
        }
        throw new PlugException("No CinemaHall with cinemaId=[ " + cinemaId + " ] filmId=[ " + filmId + " ] sessionId=[ " + sessionId + " ]");
    }

    public static List<FilmDetailInfoSessions> getWithNoSessions(String cinemaId,
                                                                 String filmId) throws PlugException {
        List<FilmDetailInfoSessions> filmDetailInfoSessionsList = new ArrayList<>();
        for (CinemaHallModel result : cinemaHallList){
            if (result.getCinemaId().equals(cinemaId) && result.getFilmId().equals(filmId)){
                FilmDetailInfoSessions filmDetailInfoSessions = new FilmDetailInfoSessions();
                filmDetailInfoSessions.setSessionId(result.getSessionId());
                filmDetailInfoSessions.setHallId(result.getHallId());
                filmDetailInfoSessions.setHallName(result.getHallName());
                filmDetailInfoSessions.setFilmTime(result.getFilmTime());
                filmDetailInfoSessionsList.add(filmDetailInfoSessions);
            }
        }
        return filmDetailInfoSessionsList;
    }

}
