package com.test.my.utils.dbutils.plug.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.*;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.ProxyCallableDB;
import com.test.my.utils.dbutils.plug.dbplug.SeatsTable;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;
import lombok.extern.log4j.Log4j2;

import javax.ws.rs.core.MultivaluedHashMap;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
public class DbReservationApi {

    //============================ MAKE RESERVATION ============================

    public void makeReserve(ReservationRequest rq, String reservationId) throws AppException {
        try{
            Map<Integer, Integer> results = executeForUpdate(rq, reservationId);

            if (results.size() != 0) {
                for (Integer affectedRow : results.keySet()){
                    if (results.get(affectedRow) == 0)
                        throw new Exception("Failed to update seats in row=[ " + affectedRow + " ]");
                }
            }else
                throw new Exception("No seats found for reservation");

        }catch (Exception e){
            log.error("Failed to get Make Reservation: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_SEAT_EXCEPTION, "Failed to point reservation seats");
        }
    }

    private Map<Integer, Integer> executeForUpdate(ReservationRequest rq, String reservationId) throws PlugException {
        Map<Integer, Integer> integerIntegerMap = new HashMap<>();
        List<ReservationRequestSeats> reservationRequestSeats = rq.getSeats();
        for (ReservationRequestSeats seat : reservationRequestSeats){
            List<Integer> request = seat.getSeatsInRow().stream()
                    .map(ReservationRequestSeatsInRow::getSeat)
                    .collect(Collectors.toList());
            integerIntegerMap.put(seat.getRow(), SeatsTable.reserveSeats(rq.getSessionId(),
                                                                                rq.getCinemaId(),
                                                                                seat.getRow(),
                                                                                reservationId,
                                                                                request));
        }
        return integerIntegerMap;
    }

    //============================ REFUSE RESERVATION ============================

    public void refuseReserve(ReservationRefuseRequest refuseRq) throws AppException {
        try{
            int results = executeForRefuse(refuseRq);

            if (results == 0)
                throw new Exception("Failed to refuse reservation=[ " + refuseRq.getReserveId() + "]");

        }catch (Exception e){
            log.error("Failed to refuse reservation=[ " + refuseRq.getReserveId() + "]", e);
            throw new AppException(AppException.Code.DB_SEAT_EXCEPTION, "Failed to refuse reservation seats");
        }
    }

    private int executeForRefuse(ReservationRefuseRequest refuseRq) throws PlugException {
        return SeatsTable.refuseReservedSeats(refuseRq.getSessionId(), refuseRq.getCinemaId(), refuseRq.getReserveId());
    }

    //============================ BLOCK FOR RESERVATION ============================

    public void pointReserve(ReservationPointRequest pointRequest) throws AppException {
        try{
            Map<Integer, Integer> results = executeForRefuse(pointRequest);

            if (results.size() != 0) {
                for (Integer affectedRow : results.keySet()) {
                    if (results.get(affectedRow) == 0)
                        throw new Exception("Failed to point seats in row=[ " + affectedRow + " ]");
                }
            }else
                throw new Exception("No seats found for point");

        }catch (Exception e){
            log.error("Failed to point reservation seats", e);
            throw new AppException(AppException.Code.DB_SEAT_EXCEPTION, "Failed to point reservation seats");
        }
    }

    private Map<Integer, Integer> executeForRefuse(ReservationPointRequest pointRequest) throws PlugException {
        Map<Integer, Integer> integerIntegerMap = new HashMap<>();
        List<ReservationRequestSeats> reservationRequestSeats = pointRequest.getSeats();
        for (ReservationRequestSeats seat : reservationRequestSeats){
            List<Integer> request = seat.getSeatsInRow().stream()
                    .map(ReservationRequestSeatsInRow::getSeat)
                    .collect(Collectors.toList());
            integerIntegerMap.put(seat.getRow(), SeatsTable.lockReservedSeats(pointRequest.getSessionId(),
                    pointRequest.getCinemaId(),
                    seat.getRow(),
                    request));
        }
        return integerIntegerMap;
    }
}
