package com.test.my.utils.dbutils.plug.exceptions;

public class PlugException extends Exception{

    public PlugException(String message, Exception e){
        super(message, e);
    }

    public PlugException(String message){
        super(message);
    }
}
