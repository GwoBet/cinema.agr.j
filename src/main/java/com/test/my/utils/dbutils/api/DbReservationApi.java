package com.test.my.utils.dbutils.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.*;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.ProxyCallableDB;
import lombok.extern.log4j.Log4j2;

import javax.persistence.criteria.CriteriaBuilder;
import javax.ws.rs.core.MultivaluedHashMap;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class DbReservationApi {
    private ResultSet resultSet;
    private ReservationRequest rq;
    private ReservationRefuseRequest refuseRq;
    private String reservationNumber;
    public static final String UPDATE_SEATS = "UPDATE seats " +
                                              "SET reservationNumber = :p$reservationNumber " +
                                              "WHERE reserved = true AND cinemaId = :p$cinemaId AND sessionId = :p$sessionId AND row = :p$row AND seat IN (:p$seatArray) ; ";
    public static final String REFUSE_RESERVATION = "UPDATE seats " +
                                                      "SET reserved = false, reservationNumber = DEFAULT " +
                                                      "WHERE cinemaId = :p$cinemaId AND sessionId = :p$sessionId AND reserveId = :p$reserveId ;";
    public static final String BLOCK_RESERVATION = "UPDATE seats " +
                                                    "SET reserved = true " +
                                                    "WHERE cinemaId = :p$cinemaId AND sessionId = :p$sessionId AND reserveId = :p$reserveId ;";

    //============================ MAKE RESERVATION ============================

    public void makeReserve(ReservationRequest rq, String reservationNumber) throws AppException {
        this.rq = rq;
        this.reservationNumber = reservationNumber;
        try{
            Map<Integer, Integer> results = executeDBForUpdate();

            for (Integer affectedRow : results.keySet()){
                if (results.get(affectedRow) == 0)
                    throw new Exception("Failed to update seats in row=[ " + affectedRow + " ]");
            }
        }catch (SQLException e){
            log.error("Failed to get Make Reservation: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Make Reservation: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]");
        }catch (Exception e){
            log.error("Failed to get Make Reservation: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Make Reservation: msg=[ " + e.getMessage() + "]");
        }
    }

    private Map<Integer, Integer> executeDBForUpdate() throws SQLException {
        MultivaluedHashMap<Integer, Integer> seats = new MultivaluedHashMap<>();
        List<ReservationRequestSeats> rqSeats = rq.getSeats();
        Map<Integer, Integer> affectedRows = new HashMap<>();
        for (ReservationRequestSeats s : rqSeats){
            seats.addAll(s.getRow(), s.getSeatsInRow().stream().map(ReservationRequestSeatsInRow::getSeat).toArray(Integer[]::new));
        }
        try(ProxyCallableDB db = new ProxyCallableDB()){
            for (int row : seats.keySet()){
                String readyExec = UPDATE_SEATS;
                db.prepare(readyExec);
                db.setString(":p$reservationNumber", reservationNumber);
                db.setString(":p$cinemaId", rq.getCinemaId());
                db.setString(":p$sessionId", rq.getSessionId());
                db.setInt(":p$row", row);
                db.setArray(":p$seatArray", "INTEGER", seats.get(row));

                affectedRows.put(row, db.executeUpdateStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout()));
                db.closeStatement();
            }
        }
        return affectedRows;
    }

    //============================ REFUSE RESERVATION ============================

    public void refuseReserve(ReservationRefuseRequest refuseRq) throws AppException {
        this.refuseRq = refuseRq;
        try{
            int results = executeDBForRefuse();

            if (results == 0)
                throw new Exception("Failed to refuse reservation=[ " + refuseRq.getReserveId() + "]");

        }catch (SQLException e){
            log.error("Failed to get Refuse Reservation: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Refuse Reservation: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]");
        }catch (Exception e){
            log.error("Failed to get Refuse Reservation: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Refuse Reservation: msg=[ " + e.getMessage() + "]");
        }
    }

    private int executeDBForRefuse() throws SQLException {
        int affectedRows = 0;
        try(ProxyCallableDB db = new ProxyCallableDB()){
            String readyExec = BLOCK_RESERVATION;
            db.prepare(readyExec);
            db.setString(":p$reservationNumber", refuseRq.getReserveId());
            db.setString(":p$cinemaId", refuseRq.getCinemaId());
            db.setString(":p$sessionId", refuseRq.getSessionId());

            affectedRows = db.executeUpdateStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout());
        }
        return affectedRows;
    }

    //============================ BLOCK FOR RESERVATION ============================

    public void blockReserve(ReservationRefuseRequest refuseRq) throws AppException {
        this.refuseRq = refuseRq;
        try{
            int results = executeDBForBlock();

            if (results == 0)
                throw new Exception("Failed to block reservation=[ " + refuseRq.getReserveId() + "]");

        }catch (SQLException e){
            log.error("Failed to get Block Reservation: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Block Reservation: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]");
        }catch (Exception e){
            log.error("Failed to get Block Reservation: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Block Reservation: msg=[ " + e.getMessage() + "]");
        }
    }

    private int executeDBForBlock() throws SQLException {
        int affectedRows = 0;
        try(ProxyCallableDB db = new ProxyCallableDB()){
            String readyExec = REFUSE_RESERVATION;
            db.prepare(readyExec);
            db.setString(":p$reservationNumber", refuseRq.getReserveId());
            db.setString(":p$cinemaId", refuseRq.getCinemaId());
            db.setString(":p$sessionId", refuseRq.getSessionId());

            affectedRows = db.executeUpdateStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout());
        }
        return affectedRows;
    }
}
