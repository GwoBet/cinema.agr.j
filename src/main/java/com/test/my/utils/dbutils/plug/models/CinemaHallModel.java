package com.test.my.utils.dbutils.plug.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CinemaHallModel {

    private String hallId;

    private String hallName;

    private String cinemaId;

    private String filmId;

    private String filmTime;

    private String sessionId;

}