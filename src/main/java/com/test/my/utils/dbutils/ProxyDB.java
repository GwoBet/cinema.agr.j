package com.test.my.utils.dbutils;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.test.my.conf.Config;
import com.test.my.exceptions.AppException;
import com.test.my.exceptions.CinemaApiException;
import com.test.my.utils.ConfigHelper;
import lombok.extern.log4j.Log4j2;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.StringTokenizer;

@Log4j2
public class ProxyDB {
  Connection conn = null;
  static ComboPooledDataSource cpds = null;

  public static void intiPDS() throws SQLException, PropertyVetoException, CinemaApiException {
    cpds = new ComboPooledDataSource();
    cpds.setDriverClass( "org.postgresql.Driver" );

    Config.Jdbc cjdbc = ConfigHelper.getInstance().getCfg().getJdbc();
    log.trace("init PoolDataSource by:" + cjdbc.getUrl());
    if (cjdbc.getUrl() == null)
      throw new CinemaApiException("Empty JDBC URL!");
    cpds.setJdbcUrl(cjdbc.getUrl());
    cpds.setUser(cjdbc.getUser());
    cpds.setPassword(cjdbc.getPassword());
    cpds.setInitialPoolSize(cjdbc.getPoolSize());
    cpds.setMaxPoolSize(cjdbc.getMaxPoolSize());
    cpds.setMaxIdleTime(cjdbc.getIdleTimeout());
    cpds.setUnreturnedConnectionTimeout(cjdbc.getConnectionWaitTimeout());

  }

  void getConnectionFromPool() throws SQLException {
    SQLException exception = null;
    for (int tryCnt = 0; tryCnt < 3; tryCnt++) {
      try {
        conn = cpds.getConnection();
        conn.setAutoCommit(false);
        return;
      } catch (SQLException ex) {
        exception = ex;
        log.warn("get connection from pool. try number: " + (++tryCnt), ex);
        try {
          Thread.sleep(300);
        } catch (InterruptedException ignore) {
        }
      }
    }
    throw exception;
  }

  public Connection getConnection() {
    return conn;
  }

  public static String userMsg(SQLException ex) {
    if (ex.toString().contains("ORA-04068")) return "Existing state of packages has been discarded";
    if (ex.toString().contains("ORA-20")) {
      String errMsg = ex.toString();
      StringTokenizer st = new StringTokenizer(errMsg, "|#");
      st.nextToken();
      if (st.hasMoreTokens()) {
        errMsg = st.nextToken();
      } else {
        st = new StringTokenizer(errMsg, "\n");
        errMsg = st.nextToken();
        int n = errMsg.indexOf(":");
        if (n != -1) {
          errMsg = errMsg.substring(n + 1);
          n = errMsg.indexOf(":");
          if (n != -1) {
            errMsg = errMsg.substring(n + 1);
          }
        }
      }
      return errMsg;
    }
    return null;
  }
}