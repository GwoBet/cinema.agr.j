package com.test.my.utils.dbutils.plug.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.CinemaHallResponse;
import com.test.my.srv.model.FilmListResult;
import com.test.my.srv.model.SeatInfo;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.ProxyCallableDB;
import com.test.my.utils.dbutils.plug.dbplug.CinemaHallTable;
import com.test.my.utils.dbutils.plug.dbplug.CinemaTable;
import com.test.my.utils.dbutils.plug.dbplug.SeatsTable;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;
import com.test.my.utils.dbutils.plug.models.CinemaHallModel;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DbCinemaHallApi {

    public CinemaHallResponse getCinemaHall(String cinemaId, String filmId, String sessionId) throws AppException {

        try{
            CinemaHallResponse result = executeForSeats(cinemaId, filmId, sessionId);

            return result;

        }catch (Exception e){
            log.error("Failed to get Cinema Hall Data: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.CINEMA_HALL_NOT_FOUND_EXCEPTION, "Failed to get Cinema Hall Data: msg=[ " + e.getMessage() + "]");
        }
    }

    private CinemaHallResponse executeForSeats(String cinemaId, String filmId, String sessionId) throws PlugException {
        CinemaHallModel cinemaHallModel = CinemaHallTable.get(cinemaId, filmId, sessionId);
        List<SeatInfo> seatInfoList = SeatsTable.getSeats(cinemaId, cinemaHallModel.getHallId(), sessionId);
        CinemaHallResponse cinemaHallResponse = new CinemaHallResponse();
        cinemaHallResponse.setHallId(cinemaHallModel.getHallId());
        cinemaHallResponse.setHallName(cinemaHallModel.getHallName());
        cinemaHallResponse.setSeats(seatInfoList);
        return cinemaHallResponse;
    }

}
