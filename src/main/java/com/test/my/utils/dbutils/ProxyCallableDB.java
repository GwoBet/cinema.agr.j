package com.test.my.utils.dbutils;

import lombok.extern.log4j.Log4j2;

import javax.xml.rpc.Call;
import java.io.Closeable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.util.*;

@Log4j2
@SuppressWarnings({"WeakerAccess", "StringConcatenationInsideStringBufferAppend", "unused"})
public class ProxyCallableDB extends ProxyDB implements Closeable {
  private CallableStatement stmt = null;
  private String sql = "-";
  private StringBuilder argsList = new StringBuilder();
  private StringBuilder respList = new StringBuilder();

  public static class ProxyOracleStruct {
    private String type;
    private Object[] values;

    public ProxyOracleStruct(String type, Object[] values) {
      this.type = type;
      this.values = values;
    }

    private Struct getStruct(ProxyCallableDB db) throws SQLException {
      List<Object> list = new ArrayList<>();
      for (Object o : values) {
        if (o instanceof ProxyOracleStruct) list.add(((ProxyOracleStruct) o).getStruct(db));
        else if (o instanceof ProxyOracleArrayOfStruct) list.add(((ProxyOracleArrayOfStruct) o).getArray(db));
        else if (o instanceof ProxyOracleArrayOfStrings) list.add(((ProxyOracleArrayOfStrings) o).getArray(db));
        else if (o instanceof BigInteger) list.add(((BigInteger) o).longValue());
        else list.add(o);
      }
      return db.getConnection().createStruct(type, list.toArray());
    }

    @Override
    public String toString() {
      return "(struct type:" + type + ")=" + Arrays.toString(values);
    }
  }

  public static class ProxyOracleArrayOfStruct {
    private String type;
    List<Object> array = new ArrayList<>();

    public ProxyOracleArrayOfStruct(String type) {
      this.type = type;
    }

    public void add(Object o) {
      array.add(o);
    }

    private Array getArray(ProxyCallableDB db) throws SQLException {
      List<Object> structs = new ArrayList<>();
      for (Object o : array) {
        if (o instanceof ProxyOracleStruct) structs.add(((ProxyOracleStruct) o).getStruct(db));
        else structs.add(o);
      }
      return db.getConnection().createArrayOf(type, structs.toArray());
    }

    @Override
    public String toString() {
      return "(array type:" + type + ")=" + array;
    }
  }

  public static class ProxyOracleArrayOfStrings {
    private String type;
    List<String> array;

    public ProxyOracleArrayOfStrings(String type, List<String> array) {
      this.type = type;
      this.array = array;
    }

    private Array getArray(ProxyCallableDB db) throws SQLException {
      return db.getConnection().createArrayOf(type, array.toArray());
    }

    @Override
    public String toString() {
      return "(array type:" + type + ")=" + array;
    }
  }

  public void prepare(String sql) throws SQLException {
    prepare(sql, true);
  }

  public void prepare(String sql, boolean needlog) throws SQLException {
    this.sql = sql;
    if (log.isTraceEnabled() && needlog) {
      String add = "";
      try {
        //add = (pds.getStatistics() == null ? "" : "connection pool statistic: " + pds.getStatistics().toString().replace("\n", " "));
      } catch (Exception ignored) {
      }
      log.trace("start: [" + sql + "] " + add);
    }
    getConnectionFromPool();
    stmt = (CallableStatement) conn.prepareStatement(sql);
    argsList = new StringBuilder();
  }

  public void registerOutParameter(int p, int t) throws SQLException {
    stmt.registerOutParameter(p, t);
  }

  public void registerOutParameter(String p, int t) throws SQLException {
    stmt.registerOutParameter(p, t);
  }

  public void registerOutStruct(String p, String type) throws SQLException {
    stmt.registerOutParameter(p, Types.STRUCT, type);
  }

  public void registerOutArray(String p, String type) throws SQLException {
    stmt.registerOutParameter(p, Types.ARRAY, type);
  }

  //-----------------------------------
  public String getString(int p) throws SQLException {
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + stmt.getString(p) + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getString:" + s);
    }
    return stmt.getString(p);
  }

  public BigDecimal getBigDecimal(String p) throws SQLException {
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + stmt.getString(p) + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getString:" + s);
    }
    return stmt.getBigDecimal(p);
  }

  public String getString(String p) throws SQLException {
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + stmt.getString(p) + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getString:" + s);
    }
    return stmt.getString(p);
  }
  public String getString(String p, boolean needLog) throws SQLException {
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + stmt.getString(p) + "] ";
      respList.append(s);
      if (log.isTraceEnabled() && needLog) log.trace("getString:" + s);
    }
    return stmt.getString(p);
  }

  public Timestamp getTimestamp(int p) throws SQLException {
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + stmt.getTimestamp(p) + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getTimestamp:" + s);
    }
    return stmt.getTimestamp(p);
  }

  public int getInt(int p) throws SQLException {
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + stmt.getInt(p) + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getInt:" + s);
    }
    return stmt.getInt(p);
  }

  public int getInt(String p) throws SQLException {
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + stmt.getInt(p) + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getInt:" + s);
    }
    return stmt.getInt(p);
  }

  public int getInt(String p, boolean needLog) throws SQLException {
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + stmt.getInt(p) + "] ";
      respList.append(s);
      if (log.isTraceEnabled() && needLog) log.trace("getInt:" + s);
    }
    return stmt.getInt(p);
  }

  public byte[] getBlob(int p) throws SQLException {
    Blob b = stmt.getBlob(p);
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[Blob sz:" + b.length() + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getBlob:" + s);
    }
    return b.getBytes(1, (int) b.length());
  }

  public byte[] getBlob(String p) throws SQLException {
    Blob b = stmt.getBlob(p);
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + (b == null ? "null" : "Blob sz:" + b.length()) + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getBlob:" + s);
    }
    return b == null ? null : b.getBytes(1, (int) b.length());
  }

  public List<String> getStringList(int p) throws SQLException {
    List<String> list = new ArrayList<>();
    try (ResultSet rs = (ResultSet) stmt.getObject(p)) {
      while (rs.next()) list.add(rs.getString(1));
    }
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + list.toString() + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getStringList:" + s);
    }
    return list;
  }

  public List<String> getStringList(String p) throws SQLException {
    List<String> list = new ArrayList<>();
    try (ResultSet rs = (ResultSet) stmt.getObject(p)) {
      while (rs.next()) list.add(rs.getString(1));
    }
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + list.toString() + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getStringList:" + s);
    }
    return list;
  }

  public List<List<String>> getArrayStringsList(String p) throws SQLException {
    List<List<String>> list = new ArrayList<>();
    Object o = stmt.getObject(p);
    if (o == null) return list;
    try (ResultSet rs = (ResultSet) o) {
      int sz = rs.getMetaData().getColumnCount();
      while (rs.next()) {
        List<String> item = new ArrayList<>();
        for (int i = 1; i <= sz; i++) item.add(rs.getString(i));
        list.add(item);
      }
    }
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=" + list.toString() + " ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getCursorArray[" + list.size() + "]:" + s);
    }
    return list;
  }

  public List<List<String>> getArrayStringsList(int p) throws SQLException {
    List<List<String>> list = new ArrayList<>();
    try (ResultSet rs = (ResultSet) stmt.getObject(p)) {
      int sz = rs.getMetaData().getColumnCount();
      while (rs.next()) {
        List<String> item = new ArrayList<>();
        for (int i = 1; i <= sz; i++) item.add(rs.getString(i));
        list.add(item);
      }
    }
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=" + list.toString() + " ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getCursorArray[" + list.size() + "]:" + s);
    }
    return list;
  }

  public List<List<Object>> getCursorArray(String p) throws SQLException {
    List<List<Object>> list = new ArrayList<>();
    try (ResultSet rs = (ResultSet) stmt.getObject(p)) {
      int sz = rs.getMetaData().getColumnCount();
      while (rs.next()) {
        List<Object> item = new ArrayList<>();
        for (int i = 1; i <= sz; i++) item.add(rs.getString(i));
        list.add(item);
      }
    }
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=" + list.toString() + " ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getCursorArray[" + list.size() + "]:" + s);
    }
    return list;
  }


  public List<Map<String, String>> getCursorListOfMaps(String p) throws SQLException {
    List<Map<String, String>> list = new ArrayList<>();
    try (ResultSet rs = (ResultSet) stmt.getObject(p)) {
      ResultSetMetaData metaData = rs.getMetaData();
      while (rs.next()) {
        Map<String, String> row = new HashMap<>();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
          row.put(metaData.getColumnName(i), rs.getString(i));
        }
        list.add(row);
      }
    }
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=[" + list.toString() + "] ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getArrayStringsMap[" + list.size() + "]:" + s);
    }
    return list;
  }



  public List<Object> getStruct(String p) throws SQLException {
    Struct struct = (Struct) stmt.getObject(p);
    List<Object> list = new ArrayList<>();
    for (Object o : struct.getAttributes()) {
      if (o instanceof Array) {
        List<Object> array = new ArrayList<>();
        ResultSet rs = ((Array) o).getResultSet();
        while (rs.next()) {
          Object item = rs.getObject(2); // первая колонка - это номер
          if (item instanceof Struct) array.add(Arrays.asList(((Struct) item).getAttributes()));
          else array.add(item);
        }
        list.add(array);
      } else list.add(o);
    }
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=" + list + " ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getStruct:" + s);
    }
    return list;
  }

  public List<List<Object>> getArrayOffStruct(String p) throws SQLException {
    List<List<Object>> list = new ArrayList<>();
    Array array = stmt.getArray(p);
    ResultSet rs = array.getResultSet();
    while (rs.next()) {
      Object item = rs.getObject(2); // первая колонка - это номер
      list.add(Arrays.asList(((Struct) item).getAttributes()));
    }
    if (log.isDebugEnabled()) {
      String s = "'" + p + "'=" + list + " ";
      respList.append(s);
      if (log.isTraceEnabled()) log.trace("getArrayOffStruct:" + s);
    }
    return list;
  }

  /*
  public Object getObject(int p) throws SQLException {
    if (log.isTraceEnabled()) log.trace("getObject(" + p + ")");
    return stmt.getObject(p);
  }*/


  //-----------------------------------
  public void setNull(int p, int t) throws SQLException {
    stmt.setNull(p, t);
    argsList.append(" '" + p + "':'Null'");
  }

  public void setTimestamp(int p, Timestamp t) throws SQLException {
    stmt.setTimestamp(p, t);
    argsList.append(" '" + p + "':'" + t.toString() + "'");
  }

  public void setDouble(int p, double v) throws SQLException {
    stmt.setDouble(p, v);
    argsList.append(" '" + p + "':'" + v + "'");
  }

  public void setObject(int p, Object v) throws SQLException {
    stmt.setObject(p, v);
    argsList.append(" '" + p + "':'" + v + "'");
  }

  public void setObject(String p, Object v) throws SQLException {
    stmt.setObject(p, v);
    argsList.append(" '" + p + "':'" + v + "'");
  }

  public void setStruct(String p, ProxyOracleStruct struct) throws SQLException {
    stmt.setObject(p, struct.getStruct(this));
    argsList.append(" '" + p + "':'").append(struct.toString()).append("'");
  }


  public void setString(int p, String val) throws SQLException {
    if (val == null) {
      stmt.setNull(p, Types.VARCHAR);
      argsList.append(" '" + p + "':'Null'");
    } else {
      stmt.setString(p, val);
      argsList.append(" '" + p + "':'" + val + "'");
    }
  }

  public void setString(String p, String val) throws SQLException {
    if (val == null) {
      stmt.setNull(p, Types.VARCHAR);
      argsList.append(" '" + p + "':'Null'");
    } else {
      stmt.setString(p, val);
      argsList.append(" '" + p + "':'" + val + "'");
    }
  }

  public void setBigInteger(String p, BigInteger val) throws SQLException {
    if (val == null) {
      stmt.setNull(p, Types.NUMERIC);
      argsList.append(" '" + p + "':'Null'");
    } else {
      stmt.setBigDecimal(p, new BigDecimal(val));
      argsList.append(" '" + p + "':'" + val + "'");
    }
  }

  public void setDate(int p, java.sql.Date d) throws SQLException {
    if (d == null) {
      argsList.append(" '" + p + "':'Null'");
      stmt.setNull(p, Types.DATE);
    } else {
      stmt.setDate(p, d);
      argsList.append(" '" + p + "':'" + d.toString() + "'");
    }
  }

  public void setLong(int p, long v) throws SQLException {
    stmt.setLong(p, v);
    argsList.append(" '" + p + "':'" + v + "'");
  }

  public void setLong(String p, long v) throws SQLException {
    stmt.setLong(p, v);
    argsList.append(" '" + p + "':'" + v + "'");
  }

  public void setInt(int p, int v) throws SQLException {
    stmt.setInt(p, v);
    argsList.append(" '" + p + "':'" + v + "'");
  }

  public void setInt(String p, Integer v) throws SQLException {
    if (v == null) {
      stmt.setNull(p, Types.NUMERIC);
      argsList.append(" '" + p + "':'Null'");
    } else {
      stmt.setInt(p, v);
      argsList.append(" '" + p + "':'" + v + "'");
    }
  }

  public void setBytes(String p, byte[] value) throws SQLException {
    argsList.append(" '" + p + "':'" + Arrays.toString(value) + "'");
    stmt.setBytes(p, (value != null) ? value : new byte[0]);
  }

  public void setArray(String p, String typeNameArray, String typeNameStruct, List<List<Object>> parts) throws SQLException {
    argsList.append(" '" + p + "':'" + parts + "'");
    List<Struct> a = new ArrayList<>();
    for (List<Object> s : parts) a.add(conn.createStruct(typeNameStruct, s.toArray()));
    stmt.setArray(Integer.parseInt(p), conn.createArrayOf(typeNameArray, a.toArray()));
  }

  /*public void setArray(String p, String typeNameArray, List<String> parts) throws SQLException {
    argsList.append(" '" + p + "':'" + parts + "'");
    stmt.setArray(Integer.parseInt(p), conn.createArrayOf(typeNameArray, parts.toArray()));
  }*/

  public void setArray(String p, String typeNameArray, List<Integer> parts) throws SQLException {
    argsList.append(" '" + p + "':'" + parts + "'");
    stmt.setArray(Integer.parseInt(p), conn.createArrayOf(typeNameArray, parts.toArray()));
  }

  public void setNumber(String p, BigInteger v) throws SQLException {
    stmt.setBigDecimal(p, v == null ? null : new BigDecimal(v));
    argsList.append(" '" + p + "':'" + v + "'");
  }

  public ResultSet executeStmt(long dtExpTime) throws SQLException {
    stmt.setQueryTimeout((int) (dtExpTime));
    return stmt.executeQuery();
  }

  public int executeUpdateStmt(long dtExpTime) throws SQLException {
    stmt.setQueryTimeout((int) (dtExpTime));
    return stmt.executeUpdate();
  }

  //================================================================================
  public void execute(long dtExpTime, boolean needLog) throws SQLException {
    if (log.isDebugEnabled() && needLog)
      log.debug("execute [QueryTimeout:" + (dtExpTime) + " sec] [" + sql + "] args:" + argsList);
    stmt.setQueryTimeout((int) (dtExpTime));
    stmt.execute();
    if (log.isTraceEnabled() && needLog) log.trace("finish: [" + sql + "] ");
  }

  public void execute(long dtExpTime) throws SQLException {
    if (log.isDebugEnabled())
      log.debug("execute [QueryTimeout:" + (dtExpTime / 1000) + " sec] [" + sql + "] args:" + argsList);
    stmt.setQueryTimeout((int) (dtExpTime / 1000));
    stmt.execute();
    if (log.isTraceEnabled()) log.trace("finish: [" + sql + "] ");
  }
  public void executeAndCommit(long dtExpTime) throws SQLException {
    executeAndCommit(dtExpTime, true);
  }

  public void executeAndCommit(long dtExpTime, boolean needLog) throws SQLException {
    execute(dtExpTime, needLog);
    commit(needLog);
  }

  public void commit() throws SQLException {
    commit(true);
  }

  public void commit(boolean needLog) throws SQLException {
    conn.commit();
    if (log.isTraceEnabled() && needLog) log.trace("committed: [" + sql + "]");
  }

  public void rollback() throws SQLException {
    conn.rollback();
    if (log.isTraceEnabled()) log.trace("rolled back: [" + sql + "]");
  }

  public void closeStatement(){
    if (stmt != null) {
      try {
        stmt.close();
        stmt = null;
      } catch (SQLException ex) {
        log.error("stmt.close():[" + sql + "]", ex);
      }
    }
  }

  @Override
  public void close() {
    if (log.isDebugEnabled() && !log.isTraceEnabled() && respList.length() != 0)
      log.debug("Result [" + sql + "] out: " + respList.toString());
    if (stmt != null) {
      try {
        stmt.close();
        stmt = null;
      } catch (SQLException ex) {
        log.error("stmt.close():[" + sql + "]", ex);
      }
    }
    if (conn != null) {
      try {
        conn.close();
        conn = null;
      } catch (SQLException ex) {
        log.error("conn.close():[" + sql + "]", ex);
      }
    }
  }
}
