package com.test.my.utils.dbutils.plug.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.FilmDetailInfo;
import com.test.my.srv.model.FilmDetailInfoSessions;
import com.test.my.srv.model.FilmInfo;
import com.test.my.srv.model.FilmListResult;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.ProxyCallableDB;
import com.test.my.utils.dbutils.plug.dbplug.CinemaHallTable;
import com.test.my.utils.dbutils.plug.dbplug.FilmTable;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;
import lombok.extern.log4j.Log4j2;
import org.glassfish.grizzly.http.util.HttpStatus;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DbFilmApi {



    public FilmListResult getFilmList(String cinemaId) throws AppException {
        try{
            FilmListResult result = executeForList(cinemaId);

            return result;

        }catch (Exception e){
            log.error("Failed to get Film Data: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_FILM_EXCEPTION, "Failed to get Film Data: msg=[ " + e.getMessage() + "]");
        }
    }

    private FilmListResult executeForList(String cinemaId){
        List<FilmInfo> filmInfoList = FilmTable.get(cinemaId);
        FilmListResult result = new FilmListResult();
        result.setFilms(filmInfoList);
        return result;
    }

    //================================= DETAIL INFORMATION =================================

    public FilmDetailInfo getFilmDetailInfo(String filmId, String cinemaId) throws AppException {
        try{
            FilmDetailInfo filmDetailInfo = executeForDetailInfo(filmId, cinemaId);

            return filmDetailInfo;

        }catch (Exception e){
            log.error("Failed to get Film Info Data: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.FILM_DETAIL_NOT_FOUND, "Failed to get Film Info Data: msg=[ " + e.getMessage() + "]");
        }
    }

    private FilmDetailInfo executeForDetailInfo(String filmId, String cinemaId) throws PlugException {
        FilmDetailInfo filmDetailInfo = FilmTable.getDetail(cinemaId, filmId);
        List<FilmDetailInfoSessions> sessionsList = CinemaHallTable.getWithNoSessions(cinemaId, filmId);
        filmDetailInfo.setSessions(sessionsList);
        return filmDetailInfo;
    }

}
