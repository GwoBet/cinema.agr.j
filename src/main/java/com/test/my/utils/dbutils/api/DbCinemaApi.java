package com.test.my.utils.dbutils.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.CinemaInfo;
import com.test.my.srv.model.CinemaResponse;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.ProxyCallableDB;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j2
public class DbCinemaApi {

    private ResultSet resultSet;
    public static final String GET_CINEMA_LIST = "SELECT * FROM cinema_table WHERE city = :p$cityFilter ";

    public CinemaResponse getCinemaList(String cityFilter, String street) throws AppException {

        try{
            executeDB(cityFilter, street);

            return mapResultSet();
        }catch (SQLException e){
            log.error("Failed to get Cinema Data: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Cinema Data: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]");
        }catch (Exception e){
            log.error("Failed to get Cinema Data: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Cinema Data: msg=[ " + e.getMessage() + "]");
        }
    }

    private void executeDB(String cityFilter, String street) throws SQLException {
        try(ProxyCallableDB db = new ProxyCallableDB()){
            String readyExec = GET_CINEMA_LIST;
            if (street != null)
                readyExec += "AND street = :p$street";
            db.prepare(readyExec);
            db.setString(":p$cityFilter", cityFilter);
            if (street != null)
                db.setString(":p$street", street);

            resultSet = db.executeStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout());
        }
    }

    private CinemaResponse mapResultSet() throws SQLException, AppException {
        CinemaResponse cinemaResponse = new CinemaResponse();
        try{
            Class<CinemaInfo> cinemaInfoClass = CinemaInfo.class;
            Field[] fields = cinemaInfoClass.getDeclaredFields();

            List<CinemaInfo> cinemaInfoList = new ArrayList<>();
            while (resultSet.next()) {
                CinemaInfo cinemaInfo = new CinemaInfo();

                for (Field field : fields) {
                    String name = field.getName();

                    String value = resultSet.getString(name);
                    field.set(cinemaInfo, field.getType().getConstructor(String.class).newInstance(value));
                }
                cinemaInfoList.add(cinemaInfo);
            }
            cinemaResponse.setCinemas(cinemaInfoList);
        }catch ( NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e){
            log.error("DbCinemaApi::mapResultSet: " + e.getClass() + " message: " + e.getMessage(), e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "DbCinemaApi::mapResultSet: " + e.getClass() + " message: " + e.getMessage());
        }
        return cinemaResponse;
    }

}
