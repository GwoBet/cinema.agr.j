package com.test.my.utils.dbutils.plug.dbplug;

import com.test.my.srv.model.*;
import com.test.my.srv.model.FilmDetailInfo;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Log4j2
public class FilmTable {

    private static String fileDelimiter = ";";

    private static List<FilmDetailInfo> filmDetailMap = Collections.synchronizedList(new ArrayList<>());

    public static void loadStorage(String filePath) throws PlugException {
        if(!new File(filePath).exists()) {
            log.trace("no Cinema file: "+filePath);
            return;
        }

        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream.forEach(s->{
                String []items = s.split(fileDelimiter);

                FilmDetailInfo filmDetailInfo = new FilmDetailInfo();
                filmDetailInfo.setFilmId(items[0]);
                filmDetailInfo.setName(items[1]);
                filmDetailInfo.setCinemaId(items[2]);
                filmDetailInfo.setStartTime(items[3]);
                filmDetailInfo.setEndTime(items[4]);
                filmDetailInfo.setFilmDuration(items[5]);
                filmDetailInfo.setFilmDescription(items[6]);
                List<FilmDetailInfoSessions> sessionsList = new ArrayList<>();
                for (int i = 7; i < items.length; i++) {
                    FilmDetailInfoSessions session = new FilmDetailInfoSessions();
                    session.setSessionId(items[i]);
                    sessionsList.add(session);
                }
                filmDetailInfo.setSessions(sessionsList);

                filmDetailMap.add(filmDetailInfo);
            });
        } catch (IOException e) {
            log.error("Can't load file '" + filePath + "'. " + e.getMessage(), e);
            throw new PlugException("Can't load file '" + filePath + "'. " + e.getMessage());
        }
    }

    public static void delete(String filmId){
        filmDetailMap.remove(filmId);
    }

    public static List<FilmInfo> get(String cinemaId) {
        List<FilmInfo> result = new ArrayList<>();
        for (FilmDetailInfo film : filmDetailMap){
            if (film.getCinemaId().equals(cinemaId)){
                FilmInfo info = new FilmInfo();
                info.setFilmId(film.getFilmId());
                info.setName(film.getName());
                info.setStartTime(film.getStartTime());
                info.setEndTime(film.getEndTime());
                info.setFilmDescription(film.getFilmDescription());
                info.setFilmDuration(film.getFilmDuration());
                info.setCinemaId(film.getCinemaId());
                result.add(info);
            }
        }
        return result;
    }

    public static FilmDetailInfo getDetail(String cinemaId, String filmId) throws PlugException {
        for (FilmDetailInfo id : filmDetailMap){
            if (id.getCinemaId().equals(cinemaId) && id.getFilmId().equals(filmId)){
                return id;
            }
        }
        throw new PlugException("No FilmDetailInfo with cinemaId=[ " + cinemaId + " ] filmId=[ " + filmId + " ]");
    }
}
