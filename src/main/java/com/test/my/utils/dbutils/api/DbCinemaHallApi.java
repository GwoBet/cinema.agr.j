package com.test.my.utils.dbutils.api;

import com.test.my.exceptions.AppException;
import com.test.my.srv.model.*;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.ProxyCallableDB;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class DbCinemaHallApi {
    private ResultSet resultSet;
    private String cinemaId;
    private String filmId;
    private String sessionId;
    public static final String GET_CINEMA_HALL_INFO = "SELECT * " +
                                                "FROM cinema_hall_table " +
                                                "WHERE filmId = :p$filmId AND cinemaId = :p$cinemaId AND sessionId = :p$sessionId;";
    public static final String GET_SEATS = "SELECT * " +
                                            "FROM seats_table " +
                                            "WHERE cinemaId = :p$cinemaId AND sessionId = :p$sessionId AND hallId = :p$hallId";

    public CinemaHallResponse getCinemaHall(String cinemaId, String filmId, String sessionId) throws AppException {
        this.cinemaId = cinemaId;
        this.filmId = filmId;
        this.sessionId = sessionId;
        try{
            executeDB();

            return mapResultSetForDetailInfo();
        }catch (SQLException e){
            log.error("Failed to get Cinema Hall: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Cinema Hall: state=[" + e.getSQLState() + "] msg=[ " + e.getMessage() + "]");
        }catch (Exception e){
            log.error("Failed to get Cinema Hall: msg=[ " + e.getMessage() + "]", e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "Failed to get Cinema Hall: msg=[ " + e.getMessage() + "]");
        }
    }


    private void executeDB() throws SQLException {
        try(ProxyCallableDB db = new ProxyCallableDB()){
            String readyExec = GET_CINEMA_HALL_INFO;
            db.prepare(readyExec);
            db.setString(":p$filmId", filmId);
            db.setString(":p$cinemaId", cinemaId);
            db.setString(":p$sessionId", sessionId);

            resultSet = db.executeStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout());
        }
    }

    private void executeDBForSeats(String hallId) throws SQLException {
        try(ProxyCallableDB db = new ProxyCallableDB()){
            String readyExec = GET_SEATS;
            db.prepare(readyExec);
            db.setString(":p$hallId", hallId);
            db.setString(":p$cinemaId", cinemaId);
            db.setString(":p$sessionId", sessionId);

            resultSet = db.executeStmt(ConfigHelper.getInstance().getCfg().getJdbc().getQueryTimeout());
        }
    }

    private CinemaHallResponse mapResultSetForDetailInfo() throws SQLException, AppException {
        CinemaHallResponse cinemaHall = new CinemaHallResponse();
        try{
            Class<CinemaHallResponse> cinemaHallClass = CinemaHallResponse.class;
            Field[] fields = cinemaHallClass.getDeclaredFields();

            Class<SeatInfo> seatInfoClass = SeatInfo.class;
            Field[] sessionFields = seatInfoClass.getDeclaredFields();

            List<SeatInfo> seatInfoList = new ArrayList<>();

            while (resultSet.next()) {
                for (Field field : fields) {
                    if (!field.getType().equals(List.class)) {
                        String name = field.getName();

                        String value = resultSet.getString(name);
                        field.set(cinemaHall, field.getType().getConstructor(String.class).newInstance(value));
                    }
                }
            }

            //=================== Get Seat List ===================
            executeDBForSeats(cinemaHall.getHallId());

            while (resultSet.next()) {
                SeatInfo seatInfo = new SeatInfo();

                for (Field field : sessionFields) {
                    String name = field.getName();

                    if (field.getType().equals(String.class)){
                        String value = resultSet.getString(name);
                        field.set(seatInfo, field.getType().getConstructor(String.class).newInstance(value));
                    }else if (field.getType().equals(Integer.class)){
                        Integer value = resultSet.getInt(name);
                        field.set(seatInfo, field.getType().getConstructor(Integer.class).newInstance(value));
                    }else if (field.getType().equals(Boolean.class)){
                        Boolean value = resultSet.getBoolean(name);
                        field.set(seatInfo, field.getType().getConstructor(Boolean.class).newInstance(value));
                    }
                }
                seatInfoList.add(seatInfo);
            }
            cinemaHall.setSeats(seatInfoList);

        }catch ( NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e){
            log.error("DbFilmApi::mapResultSet: " + e.getClass() + " message: " + e.getMessage(), e);
            throw new AppException(AppException.Code.DB_CINEMA_EXCEPTION, "DbFilmApi::mapResultSet: " + e.getClass() + " message: " + e.getMessage());
        }
        return cinemaHall;
    }

}
