package com.test.my.exceptions;

import com.test.my.srv.model.ErrorResultData;
import lombok.Getter;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class AppException extends Exception {
  public enum Code {
    DB_CINEMA_EXCEPTION(2),

    CINEMA_HALL_NOT_FOUND_EXCEPTION(11),

    FILM_DETAIL_NOT_FOUND(20),
    DB_FILM_EXCEPTION(21),

    NO_SEAT(50),
    DB_SEAT_EXCEPTION(51),

    UNDEFINED_ERROR(-100);

    private final int code;

    Code(int code) {
      this.code = code;
    }

    public int getValue() {
      return code;
    }
  }

  @Getter private int httpStatus;
  @Getter private int code;
  @Getter private String addInfo;
  @Getter private String errMsg;

  private static final String commonErrorMsg = "Internal error";

  private static final List<AppException> errorList = new ArrayList<AppException>() {
    {
      add(new AppException(Code.DB_CINEMA_EXCEPTION, 500, "Ошибка базы данных при запросе списка кинотеатров"));

      add(new AppException(Code.CINEMA_HALL_NOT_FOUND_EXCEPTION, 404, "Ошибка базы данных при запросе списка кинотеатров"));

      add(new AppException(Code.FILM_DETAIL_NOT_FOUND, 404, "Детальная информация по фильму не найдена"));
      add(new AppException(Code.DB_FILM_EXCEPTION, 500, "Ошибка базы данных при запросе списка кинотеатров"));

      add(new AppException(Code.NO_SEAT, 404, "Не найдено одно или несколько мест из списка"));
      add(new AppException(Code.DB_SEAT_EXCEPTION, 500, "Ошибка базы данных при запросе списка кинотеатров"));
    }
  };

  private AppException(Code code, int httpStatus, String msg) {
    super();
    this.errMsg = msg;
    this.httpStatus = httpStatus;
    this.code = code.getValue();
  }

  AppException errMsg(String errMsg) {
    if (errMsg != null) this.errMsg = errMsg.replace('|', ' ');
    else this.errMsg = "";
    return this;
  }

  public AppException(Code code, String addInfo) {
    super();
    this.addInfo = addInfo;
    for (AppException e : errorList) {
      if (e.code == code.getValue()) {
        this.code = e.code;
        this.errMsg = e.errMsg;
        this.httpStatus = e.httpStatus;
        return;
      }
    }
    throw new IllegalArgumentException("Internal error. Undefined error code");
  }

  public AppException(Code code) {
    this(code, null);
  }

  @Override
  public String getMessage() {
    return errMsg;
  }

  @Override
  public String toString() {
    String s = "[" + code + "] msg=[" + errMsg + "]";
    if (httpStatus > 0) s += " http status=[" + httpStatus + "]";
    if (addInfo != null) s += " addInfo=[" + addInfo + "]";
    return s;
  }

  public Response toResponse() {
    ErrorResultData err = new ErrorResultData();
    err.setCode(getCode());
    err.setMessage(getMessage());
    err.setAddInfo(getAddInfo());
    return Response.status(getHttpStatus()).entity(err).type(MediaType.APPLICATION_JSON).build();
  }

  public static String makeDocYAML() {
    final StringBuilder sb = new StringBuilder();
    errorList.forEach(err -> sb.append(String.format("\n    %4d | %3d | %s", err.code, err.httpStatus, err.errMsg)));
    return sb.toString();
  }

}
