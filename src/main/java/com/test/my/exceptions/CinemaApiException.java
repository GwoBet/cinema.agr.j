package com.test.my.exceptions;

public class CinemaApiException extends Exception {

    public CinemaApiException(String str) {
        super(str);
    }
}
