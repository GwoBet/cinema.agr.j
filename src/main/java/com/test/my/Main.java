package com.test.my;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.test.my.api.*;
import com.test.my.api.customize.MarshallingFeature;
import com.test.my.conf.Config;
import com.test.my.utils.ConfigHelper;
import com.test.my.utils.dbutils.plug.api.DbCinemaApi;
import com.test.my.utils.dbutils.plug.api.DbCinemaHallApi;
import com.test.my.utils.dbutils.plug.api.DbFilmApi;
import com.test.my.utils.dbutils.plug.api.DbReservationApi;
import com.test.my.utils.dbutils.plug.dbplug.CinemaHallTable;
import com.test.my.utils.dbutils.plug.dbplug.CinemaTable;
import com.test.my.utils.dbutils.plug.dbplug.FilmTable;
import com.test.my.utils.dbutils.plug.dbplug.SeatsTable;
import com.test.my.utils.dbutils.plug.exceptions.PlugException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.cli.*;
import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.internal.guava.ThreadFactoryBuilder;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Log4j2
public class Main {
    private static final String p_configFile = "config";
    private static final String p_reload_cfg = "reload-cfg";
    private static final String defaultConfigFile = "config.xml";


    private static CommandLine parseArgs(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("h", "help", false, "show help.");
        options.addOption("v", "version", false, "show version");
        options.addOption("c", p_configFile, true, "xml config file");
        options.addOption("r", p_reload_cfg, true, "try to reload config period");
        CommandLineParser parser = new DefaultParser();
        CommandLine line = parser.parse(options, args);
        if (line.hasOption("help")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar cinema.arg.jar", "", options, "ALEX(c)");
            System.exit(0);
        }
        if (line.hasOption("version")) {
            System.out.println(com.test.my.Version.version);
            System.exit(0);
        }
        return line;
    }

    private static void initMiddleware(Config cfg) throws PlugException {
        CinemaTable.loadStorage(cfg.getTest().getCinemasFile());
        FilmTable.loadStorage(cfg.getTest().getFilmsFile());
        CinemaHallTable.loadStorage(cfg.getTest().getCinemaHallFile());
        SeatsTable.loadStorage(cfg.getTest().getSeatsFile());
    }

    public static void main(String[] args) {
        try {
            CommandLine line = parseArgs(args);
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                log.info("============ STOP ================");
            }));
            log.info("============ START " + com.test.my.Version.version + " ================");
            Config cfg = ConfigHelper.setInstance(line.getOptionValue(p_configFile, defaultConfigFile)).getCfg();
            ConfigHelper.startReLoader(Integer.parseInt(line.getOptionValue(p_reload_cfg, "20")));
//      if(log.isTraceEnabled()) log.trace("YAML error description:"+ AppException.makeDocYAML());
            initMiddleware(cfg);
//==============================================================================
            URI uri = URI.create(cfg.getListenWeb().getUrl());
            log.info("Start web listen: " + uri.toString());
            final ResourceConfig rc = new ResourceConfig()
                    .register(MarshallingFeature.class)

                    .register(CinemaApi.class)
                    .register(CinemaHallApi.class)
                    .register(FilmApi.class)
                    .register(ReservationApi.class)

                    .register(JSONExceptionMapper.class)
                    .register(AppExceptionMapper.class)
                    .register(AllExceptionMapper.class)

                    .register(VersionApi.class)

                    .register(new AbstractBinder() {
                        @Override
                        protected void configure() {
                            bind(new DbCinemaApi()).to(DbCinemaApi.class);
                            bind(new DbFilmApi()).to(DbFilmApi.class);
                            bind(new DbCinemaHallApi()).to(DbCinemaHallApi.class);
                            bind(new DbReservationApi()).to(DbReservationApi.class);
                        }
                    })
                    .register(new JacksonJsonProvider())
                    .registerResources();
            if (log.isTraceEnabled())
                rc.register(LogTraceFilter.class);

            HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(uri, rc, false);
            httpServer.getServerConfiguration().addHttpHandler(
                    new CLStaticHttpHandler(HttpServer.class.getClassLoader(), "/webapi-doc/"),
                    "/webapi-doc/");

            NetworkListener networkListener = httpServer.getListeners().iterator().next();
            networkListener.getTransport().getWorkerThreadPoolConfig()
                    .setCorePoolSize(cfg.getListenWeb().getCoreThreadsPoolSize())
                    .setMaxPoolSize(cfg.getListenWeb().getMaxThreadsPoolSize())
                    .setKeepAliveTime(cfg.getListenWeb().getKeepAliveTime(), TimeUnit.SECONDS)
                    .setThreadFactory((new ThreadFactoryBuilder()).setNameFormat("http-srv-%d").build())
                    .setQueueLimit(-1);
            log.trace("networkListener:" + networkListener.getTransport().getWorkerThreadPoolConfig());
            httpServer.start();
        } catch (Exception | Error ex) {
            System.out.println("Fatal stop: " + ex.toString());
            log.fatal("", ex);
        }
    }
}
