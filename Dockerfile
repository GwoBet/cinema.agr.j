FROM openjdk:8

LABEL maintainer="alex.sichyov@gmail.com"

VOLUME /tmp

EXPOSE 8099

COPY testStorages/ testStorages/

ARG JAR_FILE=target/cinema.agr.j-1.0-SNAPSHOT.jar

ADD ${JAR_FILE} rebounder-chain-backend.jar

ARG CFG_FILE=/config.xml

ADD ${CFG_FILE} cinema-agr-config.xml

ARG LOG_CFG_FILE=/log4j2.xml

ADD ${LOG_CFG_FILE} cinema-agr-log4j2.xml

ENTRYPOINT ["java", "-server", "-Dfile.encoding=UTF-8", "-Djava.util.logging.manager=org.apache.logging.log4j.jul.LogManager", "-Dlog4j.configurationFile=file:/cinema-agr-log4j2.xml", "-jar","/rebounder-chain-backend.jar", "--config=/cinema-agr-config.xml"]