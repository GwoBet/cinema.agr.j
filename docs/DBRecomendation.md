## Рекомендации к Базе Данных

В данном файле описано, какие столбцы желательны к пресудствию в Базе Данных.

### CINEMA_TABLE

Таблица с данными о кинотеатрах.

Пример:

cinemaId | cinemaName | workTime | city | street | building 
------------ | ------------- | ------------- | ------------- | ------------- | -------------
C001 | Победа | 09:00-22:00 | Nsk | Ленина | 7
C002 | Киномир Эдем | 09:00-23:00 | Nsk | Кутателадзе | 4/4
... | ... | ... | ... | ... | ...

### FILM_TABLE

Таблица с данными о фильмах.

Пример:

filmId | filmName | cinemaId | startTime | endTime | filmDuration | filmDescription | sessionId
------------ | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | -------------
FM001 | Demon Slayer | C001 | 01-05-2021 | 01-06-2021 | 02:30 | Anime Adaptation of manga "Demon Slayer" | SES0001, SES0002
FM001 | Demon Slayer | C002 | 01-05-2021 | 01-06-2021 | 02:30 | Anime Adaptation of manga "Demon Slayer" | SES0005, SES0006
FM002 | Nobody | C001 | 12-05-2021 | 12-06-2021 | 02:30 | John Wick, but it's actually "Better Call Soul" in "Leviathan" reality | SES0003, SES0004, SES0007
... | ... | ... | ... | ... | ... | ... | ... 

### CINEMA_HALL_TABLE

Таблица с данными о кинозала.

Пример:

hallId | hallName | cinemaId | filmId | filmTime | sessionId
------------ | ------------ | ------------ | ------------ | ------------ | ------------
CH001 | Hall 1 | C001 | FM001 | 11:00 | SES0001
CH001 | Hall 1 | C001 | FM002 | 14:00 | SES0003
CH001 | Hall 1 | C001 | FM002 | 16:00 | SES0007
CH002 | Hall 2 | C001 | FM001 | 14:00 | SES0002
CH002 | Hall 2 | C001 | FM002 | 11:00 | SES0004
CH003 | Hall 1 | C002 | FM001 | 11:00 | SES0005
CH004 | Hall 2 | C002 | FM001 | 14:00 | SES0006
... | ... | ... | ... | ... | ... 

### SEATS_TABLE

Таблица с данными о местах.

Пример:

row | seat |reserved | sessionId | hallId | cinemaId | reservationId
------------ | ------------ |------------ | ------------ | ------------ | ------------ | ------------
0 | 0 | 0 | SES0001 | CH001 | C001
0 | 1 | 0 | SES0001 | CH001 | C001
0 | 2 | 0 | SES0001 | CH001 | C001
1 | 0 | 0 | SES0001 | CH001 | C001
1 | 1 | 0 | SES0001 | CH001 | C001
1 | 2 | 0 | SES0001 | CH001 | C001
1 | 3 | 0 | SES0001 | CH001 | C001
2 | 0 | 0 | SES0001 | CH001 | C001
2 | 1 | 0 | SES0001 | CH001 | C001
... | ... | ... | ... | ... | ...